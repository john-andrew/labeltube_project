require("!style-loader!css-loader!../css/style.css");
var React = require('react');
var ReactDOM = require('react-dom');
var routes = require('../../react_labeltube/config/routes');

ReactDOM.render(routes, document.getElementById('react-app'));