var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.base.config.js')

config.output['path'] = require('path').resolve('./assets/bundles/prod/');
config.output['filename'] = '[name]-[hash].js';
config.output['publicPath'] = '/assets/bundles/prod/'

config.plugins = config.plugins.concat([
	new BundleTracker({filename: './webpack-stats-prod.json'}),

	// globals
	new webpack.DefinePlugin({
		'process.env': {
			'SERVER_ENV': JSON.stringify('production'),
			'BASE_API_URL': JSON.stringify('https://choptube.herokuapp.com/api/'),
	}}),

	// minify code
	new webpack.optimize.UglifyJsPlugin({
		compressor: {
			warnings: false
		}
	})
]);

config.module.loaders.push(
	{test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader',}
);
config.module.loaders.push(
	{test: /\.css$/, exclude: /node_modules/, loader: 'css-loader',}
);

module.exports = config;