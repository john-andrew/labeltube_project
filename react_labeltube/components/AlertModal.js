var React = require('react');
var PropTypes = React.PropTypes;

var Modal = require('react-modal').default;


function AlertModal(props) {

	var events = {
		handleCloseModal: function(e) {
			props.closeModal(e);
		}
	}

	var baseClassMain = 'Modal-content'
	var baseErrorClass = 'Modal-Alert' + '-' + props.classExt;
	var fullBaseClass = baseClassMain + ' ' + baseErrorClass;

	return (
		<div>
			<Modal
				className={{
					base: fullBaseClass
				}}
				overlayClassName={{
					base: 'Modal-overlay'
				}}
				onRequestClose={events.handleCloseModal}
				contentLabel="Modal"
				isOpen={props.shouldRender}
			>
				{props.message}
				<br />
				<br />
				<br />
				<button
					className="Modal-button"
					onClick={events.handleCloseModal}
				>
					Close
				</button>
			</Modal>
		</div>
	)
};

// AlertModal.propTypes = {

// };

module.exports = AlertModal;