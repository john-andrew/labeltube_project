var React = require('react');
var PropTypes = React.PropTypes;

var ChopperItem = require('./ChopperItem');


function ChopperList(props) {
	var chopIndexes = props.chopIndexes;

	if (chopIndexes.length < 1) {
		return (
			<div className="ChopperList">
				{'(Add your first Chop!)'}
			</div>
		);
	}

	return (
		<div className="ChopperList">
			<ul>
				{chopIndexes.map(function(chopId, idx) {
					return (
						<ChopperItem
							key={idx}
							selectActiveChop={props.selectActiveChop}
							setEditedChop={props.setEditedChop}
							setHoveredChop={props.setHoveredChop}
							activeChop={props.activeChop}
							chop={props.chops[chopId]}
							editedChop={props.editedChop}
							hoveredChop={props.hoveredChop} />
					);
				})}
			</ul>
		</div>
	)
};

// ChopperList.propTypes = {

// };

module.exports = ChopperList;