var React = require('react');
var PropTypes = React.PropTypes;

var ChopUtils = require('../utils/models/ChopUtils');

function ChopperInput(props) {

	var events = {
		handleChange: function(e) {
			props.updateChopperInput(e);
		},
		handleAddButton: function(e) {
			props.addChop(e);
		},
		handleUpdateButton: function(e) {
			props.updateChop(e);
		},
		handleCancelButton: function(e) {
			props.cancelChopEdit(e);
		},
		handleDeleteButton: function(e) {
			props.deleteChop(e);
		}
	}

	var inputs = props.chopInputStates;
	var editedChop = props.editedChop;

	var placeholder = "Enter Chop title (100 chars max)"; 
	if (editedChop) {
		placeholder = editedChop.name;
	}

	// Add and Update buttons
	var canSubmitChop = true;
	var startMillis = ChopUtils.convertTimeToMillis(
		parseInt( ChopUtils.zeroEmptyInput(inputs.startHours)),
		parseInt( ChopUtils.zeroEmptyInput(inputs.startMins)),
		parseInt( ChopUtils.zeroEmptyInput(inputs.startSecs)),
	);
	var endMillis = ChopUtils.convertTimeToMillis(
		parseInt( ChopUtils.zeroEmptyInput(inputs.endHours)),
		parseInt( ChopUtils.zeroEmptyInput(inputs.endMins)),
		parseInt( ChopUtils.zeroEmptyInput(inputs.endSecs)),
	);
	
	if (startMillis > endMillis) {
		canSubmitChop = false;
	}
	else if (endMillis == 0) {
		canSubmitChop = false;
	}
	if (!inputs.name.trim()) {
		canSubmitChop = false;
	}

	var chopperInputEditedClass = "";
	if (props.editedChop) {
		chopperInputEditedClass = " ChopperInput-edited";
	}

	var addButton, updateButton;
	if (canSubmitChop) {
		addButton =
			<button
				onClick={events.handleAddButton}
				type="submit" >
				Add
			</button>
		updateButton =
			<button
				onClick={events.handleUpdateButton}
				type="submit" >
				Update
			</button>
	}
	else {
		addButton =
			<button
				id="ChopperInput-btn-cant-submit">
				Add
			</button>
		updateButton =
			<button
				id="ChopperInput-btn-cant-submit">
				Update
			</button>
	}

	return (
		<div className={"ChopperInput" + chopperInputEditedClass}>
			<input
				id="ChopperInput-name"
				onChange={events.handleChange}
				data-field="name"
				maxLength="100"
				placeholder={placeholder}
				type="text"
				value={inputs.name} />

			<br />
			<div className="ChopperInput-timeLabel">Start</div>
			<div className="ChopperInput-timeInput">
				<input
					onChange={events.handleChange}
					data-field="startHours"
					maxLength="2"
					placeholder="hrs"
					type="text"
					value={inputs.startHours} />
				:
				<input
					onChange={events.handleChange}
					data-field="startMins"
					maxLength="2"
					placeholder="mins"
					type="text"
					value={inputs.startMins} />
				:
				<input
					onChange={events.handleChange}
					data-field="startSecs"
					maxLength="2"
					placeholder="secs"
					type="text"
					value={inputs.startSecs} />
			</div>

			<br />

			<div className="ChopperInput-timeLabel">End</div>
			<div className="ChopperInput-timeInput">
				<input
					onChange={events.handleChange}
					data-field="endHours"
					maxLength="2"
					placeholder="hrs"
					type="text"
					value={inputs.endHours} />
				:
				<input
					onChange={events.handleChange}
					data-field="endMins"
					maxLength="2"
					placeholder="mins"
					type="text"
					value={inputs.endMins} />
				:
				<input
					onChange={events.handleChange}
					data-field="endSecs"
					maxLength="2"
					placeholder="secs"
					type="text"
					value={inputs.endSecs} />
			</div>

			<br />

			{(props.editedChop)
				?
					<div>
						{updateButton}
						<button
							className="ChopperInput-btn-submit"
							onClick={events.handleCancelButton}
							type="submit" >
							Cancel
						</button>
						<button
							className="ChopperInput-btn-submit btn-danger"
							onClick={events.handleDeleteButton}
							type="submit" >
							Delete
						</button>
					</div>
				:
					addButton
			}
		</div>
	)
};

// ChopperInput.propTypes = {

// };

module.exports = ChopperInput;