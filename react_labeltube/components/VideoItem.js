var React = require('react');
var PropTypes = React.PropTypes;
var Link = require('react-router').Link; 

var ENTER_KEY = 13;
var ESCAPE_KEY = 27;


function parseDate(date) {
	var pieces = date.split('-');
	var year = pieces[0];
	var month = pieces[1];
	var day = (pieces[2].split('T'))[0];

	return month + '-' + day + '-' + year;
}

function VideoItem(props) {
	var video = props.video;

	var events = {
		handleClickField: function(e) {
			props.editVideoItemField(e);
		},

		handleChangeField: function(e) {
			props.updateVideoItemField(e);
		},

		handleBlurField: function(e) {
			props.saveVideoItemEdit(e, props.video);
		},

		handleKeyDownField: function(e) {
			if (e.which === ENTER_KEY) {
				props.saveVideoItemEdit(e, props.video);
			}
			else if (e.which === ESCAPE_KEY) {
				props.cancelVideoItemEdit(e);
			}
		},

		handleClickDelete: function(e) {
			props.deleteVideoItem(e, props.video)
		}
	}

	return (
		<div className="grid">
		<div className="VideoItem">
			
			<div className="grid VideoItem-col-thumb">
				<img src={video.thumb_url}/>
			</div>{/*

			*/}<div className="grid VideoItem-col-info">
				{/* Name */}
				{(props.editingVideoItemId === 'VideoItem-name-' + props.idx)
					?
						<input
							id={'VideoItem-name-' + props.idx}
							onBlur={events.handleBlurField}
							onChange={events.handleChangeField}
							onKeyDown={events.handleKeyDownField}
							data-field="name"
							type="text"
							placeholder={video.name} />
					:
						<label
							className="VideoItem-label-name"
							onClick={events.handleClickField}
							htmlFor={'VideoItem-name-' + props.idx}>
							{((video.name).length >= 50)
								?
									(video.name).substring(0,51)+'...'
								:
									video.name
							}
						</label>
				}
				<br />
				
				{/* Video URL */}
				{(props.editingVideoItemId === 'VideoItem-videoURL-' + props.idx)
					?
						<input
							id={'VideoItem-videoURL-' + props.idx}
							onBlur={events.handleBlurField}
							onChange={events.handleChangeField}
							onKeyDown={events.handleKeyDownField}
							data-field="videoURL"
							type="text"
							placeholder={video.video_url} />
					:
						<label
							className="VideoItem-label-videoURL"
							onClick={events.handleClickField}
							htmlFor={'VideoItem-videoURL-' + props.idx}>
							{video.video_url}
						</label>
				}
				<br />
				{/* Project */}
					<Link
						className="VideoItem-project-link"
						to={"/chopper/" + props.video.id}>
						<button className="VideoItem-btn-project">Project</button>
					</Link>
			</div>{/*

			*/}<div className="grid VideoItem-col-dates">
				{/* Created */}
				<label className="VideoItem-label-date">
					{parseDate(video.created)} (created)
				</label>
				<br/>

				{/* Last Used */}
				<label className="VideoItem-label-date">
					{parseDate(video.last_used)} (last used)
				</label>
			</div>{/*

			*/}<div className="grid VideoItem-col-del">
				{/* Delete */}
				<button
					className="VideoItem-btn-delete btn-danger"
					onClick={events.handleClickDelete}
					type="button">
					X
				</button>
			</div>
		
		</div>
		</div>
	)
};

VideoItem.propTypes = {
	'cancelVideoItemEdit': PropTypes.func.isRequired,
	'deleteVideoItem': PropTypes.func.isRequired,
	'editVideoItemField': PropTypes.func.isRequired,
	'saveVideoItemEdit': PropTypes.func.isRequired,
	'updateVideoItemField': PropTypes.func.isRequired,
	'idx': PropTypes.number.isRequired,
	'video': PropTypes.object.isRequired,
};

module.exports = VideoItem;