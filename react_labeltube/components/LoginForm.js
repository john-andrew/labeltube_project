var React = require('react');
var PropTypes = React.PropTypes;


function LoginForm(props) {

	var events = {
		handleUsernameChange: function(e) {
			props.updateUsername(e);
		},

		handlePasswordChange: function(e) {
			props.updatePassword(e);
		},

		handleSubmit: function(e) {
			props.loginUser(e, isGuest=false);
		},

		handleSubmitGuest: function(e) {
			props.loginUser(e, isGuest=true);
		}
	}

	return (
		<div className="LoginForm">
			<form onSubmit={events.handleSubmit}>
				<h2>Login to ChopTube</h2>
				<input
					onChange={events.handleUsernameChange}
					placeholder="username"
					type="text"
					value={props.username} />
				<input
					onChange={events.handlePasswordChange}
					placeholder="password"
					type="password"
					value={props.password} />
				{(!props.username || !props.password)
					?	
						<button
							className="LoginContainer-btn-disabled"
							type="button" >
							Login
						</button>
					:
						<button
							type="submit" >
							Login
						</button>
				}
						<button
							onClick={events.handleSubmitGuest}
							value="guest"
							type="button" >
							Guest
						</button>

				{(props.invalidLoginError)
					?
						<div className="LoginContainer-errorMsg">
							Invalid username or password
						</div>
					:
						<div className="LoginContainer-errorMsg">

						</div>
				}
			</form>
		</div>
	)
};

LoginForm.propTypes = {
	loginUser: PropTypes.func.isRequired,
	updateUsername: PropTypes.func.isRequired,
	updatePassword: PropTypes.func.isRequired,
	username: PropTypes.string.isRequired,
	password: PropTypes.string.isRequired,
};

module.exports = LoginForm;