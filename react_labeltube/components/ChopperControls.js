var React = require('react');
var PropTypes = React.PropTypes;


// Given milliseconds, converts to H:M:S format
function millisToHMS(millis) {
	millis /= 1000;

	var h = Math.floor(millis / 3600);
	var m = Math.floor(millis % 3600 / 60);
	var s = Math.floor(millis % 3600 % 60);

	return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function ChopperControls(props) {

	var events = {
		handleClickPlayVideo: function (e) {
			props.playWholeVideo(e);
		},

		handleClickStart: function (e) {
			props.startPlayerFromChop(e);
		},

		handleClickContinue: function (e) {
			props.continuePlayer(e);
		},

		handleClickPause: function (e) {
			props.pausePlayer(e);
		}
	}

	chop = props.activeChop;

	var blinkPlayVideoButtonClass = "";
	var blinkPlayChopButtonClass = "";
	var activeChopTextClass = "";

	if (props.hasChopStarted) {
		blinkPlayChopButtonClass = " blink";
		activeChopTextClass = " ChopperControls-activeChopText";
	}
	if (props.hasWholeVideoStarted) {
		blinkPlayVideoButtonClass = " blink";
	}

	return (
		<div className="ChopperControls">

			{/* CHOP INFO */}
			<div className="ChopperControls-info-wrapper">
				<div className="grid-wrapper-ChopperControls-info">

					<div className="col col-ChopperControls-name">
						{(props.activeChop)
							?
							<span className={"ChopperControls-name" + activeChopTextClass}>
								{chop.name}
							</span>
							:
							<span className="ChopperControls-name">
								No chop is currently selected
							</span>
						}
					</div>{/*
			 */}<div className="col col-ChopperControls-time">
						{(props.activeChop)
							?
							<span className={"ChopperControls-time" + activeChopTextClass}>
								{millisToHMS(chop.start) + " - " + millisToHMS(chop.end)}
							</span>
							:
							null
						}
					</div>

				</div>
			</div>

			{/* PLAYER BUTTONS */}
			<div className="ChopperControls-buttons-wrapper">
				<button
					className={"ChopperControls-btn" + blinkPlayVideoButtonClass}
					onClick={events.handleClickPlayVideo}
					type="button">
					&#9654;&nbsp;Video
				</button>
				{(props.activeChop)
					?
					<button
						id="ChopperControls-btn-chop"
						className={"ChopperControls-btn" + blinkPlayChopButtonClass}
						onClick={events.handleClickStart}
						type="button">
						&#9654;&nbsp;Chop
						</button>
					:
					<button
						id="ChopperControls-btn-chop-disabled"
						className={"ChopperControls-btn"}
						type="button">
						&#9654;&nbsp;Chop
						</button>
				}
			</div>

		</div>
	)
};

// ChopperControls.propTypes = {

// };

module.exports = ChopperControls;