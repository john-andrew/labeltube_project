var React = require('react');
var PropTypes = React.PropTypes;


function SignupForm(props) {

	var events = {
		handleUsernameChange: function(e) {
			props.updateNewUsername(e);
		},

		handlePasswordChange: function(e) {
			props.updateNewPassword(e);
		},

		handlePasswordConfirmationChange: function(e) {
			props.updateNewPasswordConfirmation(e);
		},

		handleSubmit: function(e) {
			props.signupUser(e);
		}
	}

	var buttonClass = ''
	var buttonType = 'submit';
	if (props.wasUserCreated) {
		buttonClass = 'LoginContainer-btn-disabled'
		buttonType = 'button';
	}

	return (
		<div className="SignupForm">
			<form onSubmit={events.handleSubmit}>
				<h3>Create a new account</h3>
				<input
					onChange={events.handleUsernameChange}
					placeholder="enter a username"
					type="text"
					value={props.newUsername} />
				<input
					onChange={events.handlePasswordChange}
					placeholder="enter a password"
					type="password"
					value={props.newPassword} />
				<input
					onChange={events.handlePasswordConfirmationChange}
					placeholder="confirm your password"
					type="password"
					value={props.newPasswordConfirmation} />
				<button
					className={buttonClass}
					type={buttonType} >
					Sign-up
				</button>
				<ul>
                    {(props.blankUsernameError)
                        ?
                            <li className="LoginContainer-errorMsg">
                                Username cannot be blank
                            </li>
                        :
                            null
                    }
                    {(props.blankPasswordError)
                        ?
                            <li className="LoginContainer-errorMsg">
                                Password cannot be blank
                            </li>
                        :
                            null
                    }
                    {(props.differentPasswordError)
                        ?
                            <li className="LoginContainer-errorMsg">
                                Password confirmation doesn't match
                            </li>
                        :
                            null
                    }
                    {(props.isUsernameTakenError)
                        ?
                            <li className="LoginContainer-errorMsg">
                                Sorry, but this username is already taken
                            </li>
                        :
                            null
                    }
				</ul>
			</form>
		</div>
	)
};

SignupForm.propTypes = {
	signupUser: PropTypes.func.isRequired,
	updateNewUsername: PropTypes.func.isRequired,
	updateNewPassword: PropTypes.func.isRequired,
    updateNewPasswordConfirmation: PropTypes.func.isRequired,
	newUsername: PropTypes.string.isRequired,
	newPassword: PropTypes.string.isRequired,
    newPasswordConfirmation: PropTypes.string.isRequired,
    blankUsernameError: PropTypes.bool.isRequired,
    blankPasswordError: PropTypes.bool.isRequired,
    differentPasswordError: PropTypes.bool.isRequired,
};

module.exports = SignupForm;