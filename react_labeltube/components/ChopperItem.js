var React = require('react');
var PropTypes = React.PropTypes;

// Given milliseconds, converts to H:M:S format
function millisToHMS(millis) {
	millis /= 1000;

	var h = Math.floor(millis / 3600);
	var m = Math.floor(millis % 3600 / 60);
	var s = Math.floor(millis % 3600 % 60);

	return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
}

function ChopperItem(props) {
	var chop = props.chop;
	var start = millisToHMS(chop.start);
	var end = millisToHMS(chop.end);

	var events = {
		handleClickChop: function(e) {
			props.selectActiveChop(e, props.chop);
		},

		handleClickEditButton: function(e) {
			props.setEditedChop(e, props.chop)
		},

		handleMouseEnterExitChop: function(e) {
			props.setHoveredChop(e, props.chop);
		}
	};

	var editButtonClass = "ChopperItem-btn-edit-hide";
	var editButtonText = "Edit";
	var editedChopClass = "";

	if (props.hoveredChop && props.hoveredChop.id == props.chop.id) {
		editButtonClass = "ChopperItem-btn-edit";
	}

	if (props.editedChop && props.editedChop.id == props.chop.id) {
		editButtonClass = "ChopperItem-btn-edit-active";
		editButtonText = "Editing...";
		editedChopClass = " ChopperItem-edited blink";
	}

	return (
		<div>
			{(props.activeChop && chop.id === props.activeChop.id)
				?
				<li
					className={"ChopperItem ChopperItem-active" + editedChopClass}
					id={'ChopperItem-' + chop.id}
					onClick={events.handleClickChop}
					onMouseEnter={events.handleMouseEnterExitChop}
					onMouseLeave={events.handleMouseEnterExitChop} >
					<span className="ChopperItem-name">{chop.name}</span>
					<br />
					<div className="ChopperItem-bottomrow-wrapper">
						{start} - {end}
						<button
							className={editButtonClass}
							onClick={events.handleClickEditButton}>
							{editButtonText}
						</button>
					</div>
				</li>
				:
				<li
					className={"ChopperItem" + editedChopClass}
					id={'ChopperItem-' + chop.id}
					onClick={events.handleClickChop}
					onMouseEnter={events.handleMouseEnterExitChop}
					onMouseLeave={events.handleMouseEnterExitChop} >
					<span className="ChopperItem-name">{chop.name}</span>
					<br />
					<div className="ChopperItem-bottomrow-wrapper">
						{start } - {end}
						<button
							className={editButtonClass}
							onClick={events.handleClickEditButton}>
							{editButtonText}
						</button>
					</div>
				</li>
			}
		</div>
	)
};

// ChopperItem.propTypes = {

// };

module.exports = ChopperItem;