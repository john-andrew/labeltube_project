var React = require('react');
var PropTypes = React.PropTypes;


function VideoInput(props) {

	var events = {
		handleNameChange: function(e) {
			props.updateNameInput(e);
		},

		handleVideoURLChange: function(e) {
			props.updateVideoURLInput(e);
		},

		handleSubmitButton: function(e) {
			props.addVideo(e);
		}
	};


	return (
		<div className="VideoInput">
			<input
				onChange={events.handleNameChange}
				placeholder="Enter video project name here..."
				type="text"
				value={props.nameInput} />
			<input
				id="VideoInput-videoURL"
				onChange={events.handleVideoURLChange}
				placeholder="Enter video URL here..."
				type="text"
				value={props.videoURLInput} />
			<br />
			<button
				className="VideoInput-btn-submit"
				onClick={events.handleSubmitButton}
				type="submit" >
				Add Video
			</button>
		</div>
	)
};

VideoInput.propTypes = {
	addVideo: PropTypes.func.isRequired,
	updateNameInput: PropTypes.func.isRequired,
	updateVideoURLInput: PropTypes.func.isRequired,
	nameInput: PropTypes.string.isRequired,
	videoURLInput: PropTypes.string.isRequired,
};

module.exports = VideoInput;