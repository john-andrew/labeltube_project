var React = require('react');
var Link = require('react-router').Link; 
var PropTypes = React.PropTypes;
var LogoutContainer = require('../containers/LogoutContainer');


function Base(props) {
	return (
		<div className="Base">
			<div className="Base-header">
			<div className="grid-wrapper-Base">
				<div className="col col-Base-logo">
					<span id="Base-logo">
						ChopTube
					</span>
				</div>{/*}
				*/}<div className="col col-Base-nav">
					<nav className="Base-nav group">
						<ul>
							<li>
								<Link to="/main"> Videos </Link>
							</li>
							<li>
								<LogoutContainer />
							</li>
						</ul>
					</nav>
				</div>
			</div>
			</div>
		{props.children}
		</div>
	)
};

module.exports = Base;