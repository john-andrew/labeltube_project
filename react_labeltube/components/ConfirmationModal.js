var React = require('react');
var PropTypes = React.PropTypes;

var Modal = require('react-modal').default;


function ConfirmationModal(props) {

	var events = {
		handleCloseModal: function(e) {
			props.closeModal(e);
		},
        handleYesButton: function(e) {
            props.confirmModal(e);
        }
	}

	var baseClassMain = 'Modal-content'
	var baseErrorClass = 'Modal-Confirmation' + '-' + props.classExt;
	var fullBaseClass = baseClassMain + ' ' + baseErrorClass;

	return (
		<div>
			<Modal
				className={{
					base: fullBaseClass
				}}
				overlayClassName={{
					base: 'Modal-overlay'
				}}
				onRequestClose={events.handleCloseModal}
				contentLabel="Modal"
				isOpen={props.shouldRender}
			>
				{props.message}
				<br />
				<br />
				<br />
				<button
					className="Modal-button"
					onClick={events.handleCloseModal}
				>
					Cancel
				</button>
				<button
					className="Modal-button Modal-button-danger"
					onClick={events.handleYesButton}
				>
					Yes
				</button>
			</Modal>
		</div>
	)
};

// ConfirmationModal.propTypes = {

// };

module.exports = ConfirmationModal;