var React = require('react');
var PropTypes = React.PropTypes;

var ChopperInput = require('./ChopperInput');
var ChopperList = require('./ChopperList');
var Loading = require('./Loading');

function ChopperOrganizer(props) {

	var events = {
		handleClickSave: function(e) {
			props.saveChops(e);
		}
	}

	if (props.isSavingChopData) {
		return (
			<div className="ChopperOrganizer">
				<Loading text="Saving..." />
			</div>
		)
	}
	else if (props.isLoadingChopData) {
		return (
			<div className="ChopperOrganizer">
				<Loading />
			</div>
		)
	}

	if (!props.chops) {
		return (
			<div className="ChopperOrganizer grid">
				Sorry, there was an error loading the chops for this project.
			</div>			
		)
	}

	return (
		<div className="ChopperOrganizer">
			<div>
				{(props.canSave)
					?
						<button
							id="ChopperOrganizer-btn-save"
							onClick={events.handleClickSave}
							type="submit">
						SAVE PROJECT
						</button>
					:
						<div>
							<button id="ChopperOrganizer-btn-cantSave" >
							SAVE PROJECT
							</button>
							<span id="ChopperOrganizer-saveNote">
								(No changes)
							</span>
						</div>
				}

			</div>
			<ChopperInput
				addChop={props.addChop}
				cancelChopEdit={props.cancelChopEdit}
				deleteChop={props.deleteChop}
				updateChop={props.updateChop}
				updateChopperInput={props.updateChopperInput}
				chopInputStates={props.chopInputStates}
				editedChop={props.editedChop}
			/>
			<ChopperList
				selectActiveChop={props.selectActiveChop}
				setEditedChop={props.setEditedChop}
				setHoveredChop={props.setHoveredChop}
				activeChop={props.activeChop}
				chops={props.chops}
				chopIndexes={props.chopIndexes}
				editedChop={props.editedChop}
				hoveredChop={props.hoveredChop} />
		</div>
	)
};

// ChopperOrganizer.propTypes = {

// };

module.exports = ChopperOrganizer;