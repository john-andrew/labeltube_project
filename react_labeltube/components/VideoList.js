var React = require('react');
var PropTypes = React.PropTypes;

var Loading = require('./Loading');
var VideoItem = require('./VideoItem');

var config = {
	'orderByType': {
		'name': 'alphabetical',
		'created': 'numerical',
		'last_used': 'numerical',
	},
}

function sort(videos, orderBy, direction) {
	var result;
	var orderByType = config.orderByType[orderBy];
	videos.sort(function(a, b) {
		var keyA, keyB;
		if (orderByType === 'alphabetical') {
			keyA = a[orderBy].toString().toUpperCase();
			keyB = b[orderBy].toString().toUpperCase();
		}
		else if (orderByType === 'numerical') {
			keyA = a[orderBy].valueOf();
			keyB = b[orderBy].valueOf();
		}

		if (keyA < keyB) {
			result = -1;
		}
		else if (keyA > keyB) {
			result = 1;
		}
		else {
			result = 0;
		}

		if (direction === 'descending') {
			result *= -1;
		}

		return result;
	});

	return videos;
}

function paginate(videos, page, displayAmount) {
	var count = videos.length;

	var b = page * displayAmount - 1;
	var a = b - displayAmount + 1;

	if (b > count) {
		b = count;
	}
	if (a < 0) {
		a = 0;
	}

	return videos.slice(a, b+1);
}


function VideoList(props) {

	var events = {
		handleClickOrder: function(e) {
			props.changeOrderBy(e);
		},
		handleClickDirection: function(e) {
			props.changeDirection(e);
		},
		handleClickPage: function(e) {
			props.changePage(e);
		}
	}

	if (props.isLoadingVideoData) {
		return (
			<div className="VideoList">
				<Loading />
			</div>
		)
	}

	if (props.videos === null) {
		// render case for empty video list
	}

	var sortedVideos = sort(props.videos, props.orderBy, props.direction);
	var currentVideos = paginate(
		sortedVideos, props.currentPage, props.displayAmount
	);
	if (currentVideos.length < 1) {
		return (
			<div className="VideoList">
				{'(Add your first video project!)'}
			</div>
		)
	}

	var totalPages = Math.ceil(props.videos.length / props.displayAmount)
	if (totalPages === 0) {
		totalPages = 1
	}
	return (
		<div className="VideoList">

			<div className="VideoList-orderAndDirection-wrapper">

				<div className="VideoList-orderController">
					<ul>
						<li
							onClick={events.handleClickOrder}
							data-orderBy="name">
							{props.orderBy === 'name'
								? <span id="VideoList-activeOrderBy">Name</span>
								: <span>Name</span>
							}
						</li>
						<li
							onClick={events.handleClickOrder}
							data-orderBy="created">
							{props.orderBy === 'created'
								? <span id="VideoList-activeOrderBy">Created</span>
								: <span>Created</span>
							}
						</li>
						<li
							onClick={events.handleClickOrder}
							data-orderBy="last_used">
							{props.orderBy === 'last_used'
								? <span id="VideoList-activeOrderBy">Last Used</span>
								: <span>Last Used</span>
							}
						</li>
					</ul>
				</div>

				<div
					className="VideoList-directionController"
					onClick={events.handleClickDirection}>
					{(props.direction === 'descending')
						? <span>ASC/<span id="VideoList-activeDirection">DESC</span> &darr;&darr;</span>
						: <span><span id="VideoList-activeDirection">ASC</span>/DESC &uarr;&uarr;</span>
					}

				</div>

			</div>

			<div className="VideoList-paginator-wrapper">
				<div
					className="VideoList-paginator-larr"
					onClick={events.handleClickPage}
					data-pgDirection="down">
					&larr;
				</div>
				<div className="VideoList-paginator-currentPage">
					{props.currentPage}
				</div>
				<div className="VideoList-paginator-pageDivider">
					/
				</div>
				<div className="VideoList-paginator-maxPage">
					{totalPages}
				</div>
				<div
					className="VideoList-paginator-rarr"
					onClick={events.handleClickPage}
					data-pgDirection="up">
					&rarr;
				</div>
			</div>

			<div>
				{currentVideos.map(function (video, idx) {
					return (
						<VideoItem
							class="VideoItem"
							key={idx}
							cancelVideoItemEdit={props.cancelVideoItemEdit}
							deleteVideoItem={props.deleteVideoItem}
							editVideoItemField={props.editVideoItemField}
							saveVideoItemEdit={props.saveVideoItemEdit}
							updateVideoItemField={props.updateVideoItemField}
							idx={idx}
							editingVideoItemId={props.editingVideoItemId}
							video={video} />
					);
				})}
			</div>

			<div className="VideoList-paginator-wrapper">
				<div
					className="VideoList-paginator-larr"
					onClick={events.handleClickPage}
					data-pgDirection="down">
					&larr;
				</div>
				<div className="VideoList-paginator-currentPage">
					{props.currentPage}
				</div>
				<div className="VideoList-paginator-pageDivider">
					/
				</div>
				<div className="VideoList-paginator-maxPage">
					{totalPages}
				</div>
				<div className="VideoList-paginator-rarr"
					className="VideoList-paginator-rarr"
					onClick={events.handleClickPage}
					data-pgDirection="up">
					&rarr;
				</div>
			</div>


		</div>
	)
};

VideoList.propTypes = {
	'cancelVideoItemEdit': PropTypes.func.isRequired,
	'changeDirection': PropTypes.func.isRequired,
	'changePage': PropTypes.func.isRequired,
	'changeOrderBy': PropTypes.func.isRequired,
	'deleteVideoItem': PropTypes.func.isRequired,
	'editVideoItemField': PropTypes.func.isRequired,
	'saveVideoItemEdit': PropTypes.func.isRequired,
	'updateVideoItemField': PropTypes.func.isRequired,
	'currentPage': PropTypes.number.isRequired,
	'direction': PropTypes.string.isRequired,
	'displayAmount': PropTypes.number.isRequired,
	'orderBy': PropTypes.string.isRequired,
	'videos': PropTypes.array
};


module.exports = VideoList;