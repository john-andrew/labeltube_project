var React = require('react');
var PropTypes = React.PropTypes;

var ReactLoading = require('react-loading').default;

function Loading(props) {

	var text = 'Loading...';

	if (props.text) {
		text = props.text;
	}

	return (
		<div className="LoadingContainer">
			{text}
			<ReactLoading
				color="#8C8C8C"
				height="30px"
				type="cylon"
				width="30px" />
		</div>
	)
};

// Loading.propTypes = {

// };

module.exports = Loading;