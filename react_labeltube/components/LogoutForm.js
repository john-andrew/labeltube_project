var React = require('react');
var PropTypes = React.PropTypes;


function LogoutForm(props) {
	return (
		<div className="LogoutForm">
			<form id="form-Logout" onSubmit={props.onSubmit}>
				<button
					className="btn-Logout"
					type="submit" >
					Logout
					</button>
			</form>
		</div>
	)
};

LogoutForm.propTypes = {
	onSubmit: PropTypes.func.isRequired,
};

module.exports = LogoutForm;