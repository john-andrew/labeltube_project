var axios = require('axios');

var auth = require('../../config/auth.js');
var settings = require('../../config/settings');


var urlConfig = {
	'base': settings.BASE_API_URL,
	'modelPath': 'chops/'
}

var staticConfig = {
	'headers': {
		'Content-Type': 'application/json; charset=utf-8',
	},
	'responseType': 'json',
};

var utils = {
	/*
		===
		API
		===
	*/
	getAll: function(videoId) {
		var url = urlConfig.base + urlConfig.modelPath;
		url += '?video_id=' + videoId
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		return axios.get(url, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				return null;
			})
	},

	save: function(createdData, updatedData, deletedData, videoId) {
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
	
		var createdPromise, updatedPromise, deletedPromise;

		// created promise (POST to list view)
		if (createdData.length > 0) {
			var createdUrl = urlConfig.base + urlConfig.modelPath;
			createdUrl += '?video_id=' + videoId;

			createdPromise = axios.post(createdUrl, createdData, config)
		}

		// updated promise (PUT to detail view)
		if (updatedData.length > 0) {
			var updatedIdsCSV = updatedData.map(function(x) {
				return x.id;
			});
			var updatedUrl = (urlConfig.base
				+ urlConfig.modelPath
				+ updatedIdsCSV
				+ '/'
			);

			updatedPromise = axios.put(updatedUrl, updatedData, config)
		}

		// deleted promise (DELETE to detail view)
		if (deletedData.length > 0) {
			var deletedIdsCSV = deletedData.map(function(x) {
				return x;
			});
			var deletedUrl = (urlConfig.base
				+ urlConfig.modelPath
				+ deletedIdsCSV
				+ '/'
			);

			deletedPromise = axios.delete(deletedUrl, config)
		}

		// promises array
		var allPromises = [];

		if (createdPromise) {
			allPromises.push(createdPromise);
		}
		if (updatedPromise) {
			allPromises.push(updatedPromise);
		}
		if (deletedPromise) {
			allPromises.push(deletedPromise);
		}

		// AJAX requests
		return axios.all(allPromises)
			.then(function(responses) {
				return responses;
			})
			.catch(function(errors) {

			})
	},

	/*
		===============
		TIME FORMATTING
		===============
	*/
	// Given hours, minutes, and seconds, returns value in milliseconds
	convertTimeToMillis: function(hours, mins, secs) {
		var extraMins = hours * 60;
		var extraSecs = extraMins*60 + mins*60
		return extraSecs*1000 + secs*1000
	},

	// Given an empty string, returns a int value of 0, else return same value
	zeroEmptyInput: function(input) {
		if (input == '') {
			return 0
		}
		return input
	},


}

module.exports = utils;