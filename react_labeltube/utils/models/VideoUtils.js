var axios = require('axios');

var auth = require('../../config/auth.js');
var settings = require('../../config/settings');


var urlConfig = {
	'base': settings.BASE_API_URL,
	'modelPath': 'videos/'
}

var staticConfig = {
	'headers': {
		'Content-Type': 'application/json; charset=utf-8',
	},
	'responseType': 'json',
};

var utils = {
	getAll: function() {
		// var url = urlConfig.base + urlConfig.modelPath + urlConfig.list;
		var url = urlConfig.base + urlConfig.modelPath;
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		return axios.get(url, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	},

	getVideo: function(id) {
		var url = (urlConfig.base
			+ urlConfig.modelPath
			+ id
			+ '/'
		);
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		return axios.get(url, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	},
 
	add: function(name, videoURL) {
		var url = urlConfig.base + urlConfig.modelPath;
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		var data = {
			'name': name,
			'video_url': videoURL,
		};
		
		return axios.post(url, data, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	},

	updateName: function(name, id) {
		var url = (urlConfig.base
			+ urlConfig.modelPath
			+ id
			+ '/'
		);
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		var data = {
			'name': name,
		};
		
		return axios.put(url, data, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	},

	updateVideoURL: function(videoURL, id) {
		var url = (urlConfig.base
			+ urlConfig.modelPath
			+ id
			+ '/'
		);
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		var data = {
			'video_url': videoURL,
		};
		
		return axios.put(url, data, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	},

	remove: function(id) {
		var url = (urlConfig.base
			+ urlConfig.modelPath
			+ id
			+ '/'
		);
		var config = staticConfig;
		config.headers.Authorization = auth.createAuthorizationHeader(
			localStorage.token
		);
		config.params = {
			'id': id,
		};
		
		return axios.delete(url, config)
			.then(function(response) {
				return response.data;
			})
			.catch(function(error) {
				
				return null;
			})
	}
}

module.exports = utils;