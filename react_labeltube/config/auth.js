var axios = require('axios');

var settings = require('./settings');

var authorizationPath = 'api-token-auth/'

function getToken(username, password) {
	var url = settings.BASE_API_URL + authorizationPath;
	var data = {
		'username': username,
		'password': password,
	};
	var headers = {
		'Content-Type': 'application/json'
	};
	return axios.post(url, data, {
		'headers': headers,
	});
}

var authorization = {
	login: function(username, password) {
		return getToken(username, password)
			.then(function(response) {
				localStorage.token = response.data.token;
				return true;
			})
			.catch(function(error) {
				console.warn(error);
				return false;
			});
	},

	logout: function() {
		delete localStorage.token;
	},

	isLoggedIn: function() {
		return !!localStorage.token;
	},

	createAuthorizationHeader: function(token) {
		return 'Token ' + token;
	},
}

module.exports = authorization;