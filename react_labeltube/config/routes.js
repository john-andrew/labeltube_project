var React = require('react');
var ReactRouter = require('react-router');
var Router = ReactRouter.Router;
var Route = ReactRouter.Route;
var hashHistory = ReactRouter.hashHistory;
var IndexRoute = ReactRouter.IndexRoute;

var auth = require('./auth')

var MainContainer = require('../containers/MainContainer');
var ChopperContainer = require('../containers/ChopperContainer');

var Home = require('../components/Home');
var Base = require('../components/Base');


// Functions
function requireAuth(nextState, replace) {
	if (!auth.isLoggedIn()) {
		replace({
			pathname: '/',
			state: {nextPathname: 'main'}
		})
	}
}


// Routes
var routes = (
	<Router history={hashHistory}>
		<Route path='/' component={Home}>
		</Route>
		<Route path='main' component={Base}>
			<IndexRoute component={MainContainer}
			            onEnter={requireAuth}/>
		</Route>
		<Route path='chopper/:video_id' component={Base}>
			<IndexRoute component={ChopperContainer}
			            onEnter={requireAuth}/>
		</Route>
	</Router>
);

module.exports = routes;