var axios = require('axios');
var React = require('react');

var auth = require('../config/auth');
var settings = require('../config/settings');

var LoginForm = require('../components/LoginForm');
var SignupForm = require('../components/SignupForm');

var urlConfig = {
	'base': settings.BASE_API_URL,
	'modelPath': 'users/'
}

var staticConfig = {
	'headers': {
		'Content-Type': 'application/json; charset=utf-8',
	},
	'responseType': 'json',
};

var LoginContainer = React.createClass({
	contextTypes: {
		router: React.PropTypes.object.isRequired
	},

	getInitialState: function() {
		return {
			username: '',
			password: '',
			invalidLoginError: false,

			newUsername: '',
			newPassword: '',
			newPasswordConfirmation: '',
			isSignupDisplayed: false,
			blankUsernameError: false,
			blankPasswordError: false,
			differentPasswordError: false,
			wasUserCreated: false,
			isUsernameTakenError: false,
			registeredUsername: ''
		}
	},

	updateUsername: function(e) {
		this.setState({
			username: e.target.value
		});
	},

	updatePassword: function(e) {
		this.setState({
			password: e.target.value
		});
	},

	updateNewUsername: function(e) {
		this.setState({
			newUsername: e.target.value,
		});
	},

	updateNewPassword: function(e) {
		this.setState({
			newPassword: e.target.value,
		});
	},

	updateNewPasswordConfirmation: function(e) {
		this.setState({
			newPasswordConfirmation: e.target.value
		});
	},

	displaySignup: function(e) {
		this.setState({
			isSignupDisplayed: true
		});
	},

	loginUser: function(e, isGuest) {
		e.preventDefault();

		var username, password;

		if (isGuest) {
			username = 'guest';
			password = 'guest';			
		}
		else {
			username = this.state.username;
			password = this.state.password;
		}


		auth.login(username, password)
			.then(function(wasLoginSuccessful) {
				if (wasLoginSuccessful) {
					this.context.router.replace('main');
				}
				else {
					this.setState({
						invalidLoginError: true
					});
				}
			}.bind(this))
	},

	signupUser: function(e) {
		e.preventDefault();

		this.setState({
			isUsernameTakenError: false
		});

		// validation
		if (!this.state.newUsername || !this.state.newPassword ||
		     (this.state.newPassword !== this.state.newPasswordConfirmation)) {
			this.setState({
				blankUsernameError: !this.state.newUsername,
				blankPasswordError: !this.state.newPassword,
				differentPasswordError: (this.state.newPassword !== this.state.newPasswordConfirmation)
			});
			return;
		}
		else {
			this.setState({
				blankUsernameError: false,
				blankPasswordError: false,
				differentPasswordError: false
			});
		}

		var url = urlConfig.base + urlConfig.modelPath;
		var config = staticConfig;
		var data = {
			'username': this.state.newUsername,
			'password': this.state.newPassword,
		};

		axios.post(url, data, config)
			.then(function(response) {
				this.setState({
					wasUserCreated: true,
					newUsername: '',
					newPassword: '',
					newPasswordConfirmation: '',
					registeredUsername: response.data.username
				})

				return;
			}.bind(this))
			.catch(function(error) {
				if (error.response.data.error && error.response.data.error === 'duplicate') {
					this.setState({
						isUsernameTakenError: true
					})
				}
				return;
			}.bind(this))

	},

	render: function() {
		return (
			<div className="LoginContainer">
			
				<div className="noteContainer">
					<hr />
					<p>
						DEVELOPER'S NOTES:
						<ul>
							<li>
								For quick notes on how the app works and other tidbits, you can check 
								the <a href="https://bitbucket.org/john-andrew/labeltube_project#readme">read me</a>
							</li>
							<li>
								Due to a possible bug in the Youtube
								Player API, the "Chop" functionality may not work
								properly unless ads are blocked (ads may cause a video to replay).
								Please turn on your ad blocker for an optimal experience, thank you!
							</li>
							<li>
								For your convenience, you can login as a Guest and view the pre-made video projects 
								prefixed with "DEMO: " to see a few use cases
							</li>
							<li>
								It goes without saying, but please do not delete the demo projects or any of their Chops.
								Thank you!
							</li>
						</ul>

					</p>
					<hr />
				</div>
				
				<LoginForm
					loginUser={this.loginUser}
					updateUsername={this.updateUsername}
					updatePassword={this.updatePassword}
					username={this.state.username}
					password={this.state.password}
					invalidLoginError={this.state.invalidLoginError} />
				{(this.state.isSignupDisplayed)
					?
						<div>
							<SignupForm
								signupUser={this.signupUser}
								updateNewUsername={this.updateNewUsername}
								updateNewPassword={this.updateNewPassword}
								updateNewPasswordConfirmation={this.updateNewPasswordConfirmation}
								newUsername={this.state.newUsername}
								newPassword={this.state.newPassword}
								newPasswordConfirmation={this.state.newPasswordConfirmation}
								blankUsernameError={this.state.blankUsernameError}
								blankPasswordError={this.state.blankPasswordError}
								differentPasswordError={this.state.differentPasswordError}
								wasUserCreated={this.state.wasUserCreated}
								isUsernameTakenError={this.state.isUsernameTakenError} />

							{(this.state.wasUserCreated)
								?
									<div className="LoginContainer-textWrapper">
										"{this.state.registeredUsername}" has been registered!
									</div>
								:
									null
							}
						</div>
					:
						<div
							className="LoginContainer-textWrapper"
							onClick={this.displaySignup}>
							<span id="LoginContainer-newUser">New user?</span>
						</div>
				}
			</div>
		)
	}
});

module.exports = LoginContainer;