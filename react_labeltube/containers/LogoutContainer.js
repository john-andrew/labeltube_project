var React = require('react');

var auth = require('../config/auth');

var LogoutForm = require('../components/LogoutForm');


var LogoutContainer = React.createClass({
	contextTypes: {
		router: React.PropTypes.object.isRequired
	},

	handleSubmit: function(e) {
		e.preventDefault();

		auth.logout();
		this.context.router.replace('/');
	},

	render: function() {
		return (
			<div className="LogoutContainer">
				<LogoutForm
					onSubmit={this.handleSubmit} />
			</div>
		)
	}
});

module.exports = LogoutContainer;