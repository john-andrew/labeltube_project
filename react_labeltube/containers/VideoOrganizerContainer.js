var React = require('react');

var VideoUtils = require('../utils/models/VideoUtils');

var VideoList = require('../components/VideoList');
var VideoInput = require('../components/VideoInput');
var AlertModal = require('../components/AlertModal');
var ConfirmationModal = require('../components/ConfirmationModal');

var config = {
		'initialOrderBy': 'created',
		'initialDirection': 'descending',
		'defaultDisplayAmount': 5,
}


var VideoOrganizerContainer = React.createClass({
	/*
		=========
		LIFECYCLE
		=========
	*/
	getInitialState: function() {
		return {
			isLoadingVideoData: true,

			// VideoOrganizerContainer
			videos: null,
			currentPage: 1,

			// VideoInput
			nameInput: '',
			videoURLInput: '',

			//VideoList
			orderBy: config.initialOrderBy,
			direction: config.initialDirection,
			displayAmount: config.defaultDisplayAmount,

			// VideoItem
			editingVideoItemId: undefined,
			videoItemFieldInput: undefined,

			//AlertModal
			alertMessage: '',
			alertClassExt: '',
			shouldRenderAlertModal: false,

			//ConfirmationModal
			confirmationMessage: '',
			confirmationClassExt: '',
			confirmationCallback: null,
			confirmationData: null,
			shouldRenderConfirmationModal: false,
		}
	},

	componentDidMount: function() {
		VideoUtils.getAll()
			.then(function(videos) {
				this.setState( {
					'videos': videos,
					'isLoadingVideoData': false,
				});	
			}.bind(this));
			
	},

	/*
		=======
		ACTIONS
		=======
	*/

	//VideoInput
	updateNameInput: function(e) {
		this.setState({
			'nameInput': e.target.value,
		});
	},
	updateVideoURLInput: function(e) {
		this.setState({
			'videoURLInput': e.target.value,
		});
	},

	addVideo: function(e) {
		var nameInput = this.state.nameInput.trim();
		var videoInput = this.state.videoURLInput.trim();

		if (!this.validators.videoName(nameInput)) {
			this.setAlertModalStates(e, 'videoNameError');
			return;
		}

		VideoUtils.add(nameInput, videoInput)
			.then(function(success) {
				if (!success) {
					this.setAlertModalStates(e, 'videoURLError');
				}
				else {
					this.setAlertModalStates(e, 'addedVideoSuccess');
					this.setState({
						nameInput: '',
						videoURLInput: '',
					});
					this.updateVideosState();
				}
			}.bind(this));
	},

	// VideoList
	changeOrderBy: function(e) {
		var target = e.currentTarget;
		var orderBy = target.getAttribute('data-orderBy');
		var prevOrderByElem = document.getElementById('VideoList-activeOrderBy').parentElement;

		if (prevOrderByElem) {
			if (prevOrderByElem === target) {
				return;
			}
		}

		this.setState({
			'orderBy': orderBy
		})
	},
	
	changePage: function(e, triggeringAction) {
		var pgDirection = '';
		var currentPage = this.state.currentPage;
		var videosLength = this.state.videos.length;
		var displayAmount = this.state.displayAmount;
		var totalPages = Math.ceil(videosLength / displayAmount);
		
		if (totalPages == 0) {
			totalPages = 1
		}

		if ((!e && triggeringAction === 'deleteVideoItem')) {
			if ((videosLength % displayAmount === 0) &&
			   (currentPage > totalPages)) {
				pgDirection = 'down'
			}
			else {
				return;
			}
		}
		else {
			pgDirection = e.currentTarget.getAttribute('data-pgDirection');
		}

		var delta;
		if (pgDirection === 'up') {
			delta = 1;
		}
		else {
			delta = -1;
		}

		var newPage = currentPage;
		newPage += delta;

		if (newPage < 1) {
			newPage = 1;
		}
		else if (newPage > totalPages) {
			newPage = totalPages;
		}

		this.setState({
			'currentPage': newPage
		});


	},

	changeDirection: function(e) {
		var currDirection = this.state.direction;
		var newDirection;

		if (currDirection === 'ascending') {
			newDirection = 'descending'
		}
		else {
			newDirection = 'ascending';
		}

		this.setState({
			'direction': newDirection
		});
	},

	// VideoList -> VideoItem
	/*
		note: since actions handle changes for multiple fields (ex:
		saveVideoItemEdit might edit either a video's name or url
		depending on which field was edited), a 'data-field' attribute is set
		on each input element in the VideoItem component so that an action knows
		which input element triggered the action, and knows whether the data
		received is for either the new video name, or new video url
	*/
	editVideoItemField: function(e) {
		var inputId = e.target.getAttribute('for');
		this.setState({
			'editingVideoItemId': inputId
		})
	},

	updateVideoItemField: function(e) {
		this.setState({
			videoItemFieldInput: e.target.value
		});
	},

	saveVideoItemEdit: function(e, video) {
		this.setState({
			'editingVideoItemId': undefined
		});

		if (e.target.value === '') {
			return;
		}

		var dataField = e.target.getAttribute('data-field');

		// name
		if (dataField === 'name') {
			if (e.target.value === video.name) {
				return;
			}

			var newName = e.target.value.trim();
			if (!this.validators.videoName(newName)) {
				this.setAlertModalStates(e, 'videoNameError');
				return;
			}

			VideoUtils.updateName(newName, video.id)
				.then(function(success) {
					this.updateVideosState();
				}.bind(this));
		}

		// video url
		else if (dataField === 'videoURL') {
			if (e.target.value === video.video_url) {
				return;
			}

			var newVideoURL = e.target.value;
			newVideoURL.trim();

			VideoUtils.updateVideoURL(newVideoURL, video.id)
				.then(function(success) {
					if (!success) {
						this.setState({
							alertMessage: 'The URL given was not a valid Youtube video URL.',
							alertClassExt: 'videoURLError',
							shouldRenderAlertModal: true
						});
					}
					else {
						this.updateVideosState();
					}
				}.bind(this));			
		}
	},

	cancelVideoItemEdit: function(e) {
		this.setState({
			'editingVideoItemId': undefined
		});
	},

	deleteVideoItem: function(e, video, confirmed) {
		if (!confirmed) {
			this.setConfirmationModalStates(
				e,
				type='deleteVideo',
				callback=this.deleteVideoItem,
				data=video
			);
			return;
		}

		this.setState({
			'editingVideoItemId': undefined
		});

		VideoUtils.remove(video.id)
			.then(function(success) {
				this.updateVideosState(function() {
					this.changePage(null, 'deleteVideoItem');
					this.setAlertModalStates(e, 'deletedVideoSuccess');
				}.bind(this));
			}.bind(this));
	},

	// Alert Modal
	closeAlertModal: function(e) {
		this.setState({
			shouldRenderAlertModal: false,
			alertMessage: '',
			alertClassExt: ''
		});
	},

	// note: classExt is the name added to the CSS class for the modal
	// rendering the error message
	setAlertModalStates: function(e, type) {
		var classExt, msg;
		switch (type) {
			case 'videoNameError':
				msg = 'The name for the video project cannot be blank.';
				classExt = "videoNameError";
				break;
			case 'videoURLError':
				msg = 'The URL given was not a valid Youtube video URL.';
				classExt = "videoURLError";
				break;
			case 'deletedVideoSuccess':
				msg = 'Video has been deleted.';
				classExt = "videoDeleted";
				break;
			case 'addedVideoSuccess':
				msg = 'Video has been added.';
				classExt = "videoAdded";
				break;
		}

		this.setState({
			alertMessage: msg,
			alertClassExt: classExt,
			shouldRenderAlertModal: true
		});
	},

	
	// Confirmation Modal
	confirmConfirmationModal: function(e) {
		this.state.confirmationCallback(e, data, confirmed=true)
		this.closeConfirmationModal(e);
	},

	closeConfirmationModal: function(e) {
		this.setState({
			shouldRenderConfirmationModal: false,
			confirmationCallback: null,
			confirmationData: null,
			confirmationMessage: '',
			confirmationClassExt: '',
		});
	},

	// note: classExt is the name added to the CSS class for the modal
	// rendering the error message
	setConfirmationModalStates: function(e, type, callback, data) {
		var classExt, msg;
		switch (type) {
			case 'deleteVideo':
				msg = 'Delete this video project permanently?';
				classExt = "deleteVideo";
				break;
		}

		this.setState({
			confirmationMessage: msg,
			confirmationClassExt: classExt,
			confirmationCallback: callback,
			confirmationData: data,
			shouldRenderConfirmationModal: true
		});
	},

	/*
		=======
		METHODS
		=======
	*/
	updateVideosState: function(callback) {
		VideoUtils.getAll()
			.then(function(videos) {
				this.setState( {
					'videos': videos,
				});
				if (callback) {
					callback();
				};
		}.bind(this));
	},
	/*
		==========
		VALIDATORS
		==========
	*/
	validators: {
		videoName: function(val) {
			if (val) {
				return true;
			}
			else {
				return false;
			}
		},
	},
	/*
		======
		RENDER
		======
	*/
	render: function() {

		return (
			<div className="VideoOrganizerContainer">
				{(this.state.shouldRenderAlertModal)
					?
						<AlertModal
							closeModal={this.closeAlertModal}
							message={this.state.alertMessage}
							classExt={this.state.alertClassExt}
							shouldRender={this.state.shouldRenderAlertModal}
						/>
					:
						null	
				}
				{(this.state.shouldRenderConfirmationModal)
					?
						<ConfirmationModal
							closeModal={this.closeConfirmationModal}
							confirmModal={this.confirmConfirmationModal}
							message={this.state.confirmationMessage}
							classExt={this.state.confirmationClassExt}
							shouldRender={this.state.shouldRenderConfirmationModal}
						/>
					:
						null	
				}
				<h1>Video Project Manager</h1>
				<VideoInput
					addVideo={this.addVideo}
					updateNameInput={this.updateNameInput}
					updateVideoURLInput={this.updateVideoURLInput}
					nameInput={this.state.nameInput}
					videoURLInput={this.state.videoURLInput}/>
				<VideoList
					cancelVideoItemEdit={this.cancelVideoItemEdit}
					changeDirection={this.changeDirection}
					changePage={this.changePage}
					changeOrderBy={this.changeOrderBy}
					deleteVideoItem={this.deleteVideoItem}
					editVideoItemField={this.editVideoItemField}
					saveVideoItemEdit={this.saveVideoItemEdit}
					updateVideoItemField={this.updateVideoItemField}
					currentPage={this.state.currentPage}
					direction={this.state.direction}
					displayAmount={this.state.displayAmount}
					editingVideoItemId={this.state.editingVideoItemId}
					isLoadingVideoData={this.state.isLoadingVideoData}
					orderBy={this.state.orderBy}
					videos={this.state.videos} />
			</div>

		)
	}
});

module.exports = VideoOrganizerContainer;