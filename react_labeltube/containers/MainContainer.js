var React = require('react');

var VideoOrganizerContainer = require('./VideoOrganizerContainer');


var MainContainer = React.createClass({
	render: function() {
		return (
			<div className="MainContainer-container">
				<VideoOrganizerContainer />
			</div>
		)
	}
});

module.exports = MainContainer;