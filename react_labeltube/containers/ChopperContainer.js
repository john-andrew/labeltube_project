var React = require('react');

var ChopperControls = require('../components/ChopperControls');
var ChopperOrganizer = require('../components/ChopperOrganizer');
var AlertModal = require('../components/AlertModal');
var ConfirmationModal = require('../components/ConfirmationModal');
var Loading = require('../components/Loading');

var ChopUtils = require('../utils/models/ChopUtils');
var VideoUtils = require('../utils/models/VideoUtils');


// Ordering configuration constants
var TIME = 0;
var NAME = 1;
var ASCENDING = 0;
var DESCENDING = 1;

// Given a youtube URL, extracts the youtube video ID and returns a string
function extractYoutubeIdFromUrl(url) {
	substringIndex = url.indexOf('youtube.com/watch?v=');
	idIndex = substringIndex + 20;

	return url.slice(idIndex, url.length);
}

var loadYT;

var ChopperContainer = React.createClass({
	/*
		=========
		LIFECYCLE
		=========
	*/
	getInitialState: function() {
		return {
			isLoadingChopData: true,
			isSavingChopData: false,
			activeWindow: 'ChopperContainer',

			// ChopperOrganizer
			chops: null,
			chopIndexes: null,
			activeChop: null,
			editedChop: null,
			hoveredChop: null,
			createdChopsKeys: {},
			updatedChopsKeys: {},
			deletedChopsKeys: {},
			orderBy: TIME,
			direction: ASCENDING,
			video: null,
			videoId: this.props.params.video_id,

			// ChopperInput
			chopNameInput: '',
			chopStartHoursInput: '',
			chopStartMinsInput: '',
			chopStartSecsInput: '',
			chopEndHoursInput: '',
			chopEndMinsInput: '',
			chopEndSecsInput: '',

			// Player
			hasChopStarted: false,
			hasChopReachedEnd: false,
			hasWholeVideoStarted: false,
			isPlayerReady: false,

			//AlertModal
			alertMessage: '',
			alertClassExt: '',
			shouldRenderAlertModal: false,

			//ConfirmationModal
			confirmationMessage: '',
			confirmationClassExt: '',
			confirmationCallback: null,
			confirmationData: null,
			shouldRenderConfirmationModal: false,
		}
	},

	componentDidMount: function() {
		// instantiates a YT.Player instance 
		// (in this container, refer to as `this.player`)
		function loadYoutubePlayer() {
			if (!loadYT) {
				loadYT = new Promise(function(resolve) {
					var tag = document.createElement('script');
					tag.src = 'https://www.youtube.com/iframe_api';
					var firstScriptTag = document.getElementsByTagName('script')[0];
					firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
					window.onYouTubeIframeAPIReady = function() {
						return resolve(window.YT);
					};
				});
			}
			loadYT.then(function(YT) {
				this.player = new YT.Player(this.playerRef, {
					height: this.props.height || 390,
					width: this.props.width || 640,
					videoId: this.props.YTid || extractYoutubeIdFromUrl(this.state.video.video_url),
					events: {
						onStateChange: this.handlePlayerStateChange,
						onReady: this.handlePlayerReady
					}
				});
			}.bind(this));
		}

		this.setUpChopsData();
		this.setUpVideoData(loadYoutubePlayer);
	},

	/*
		=======
		ACTIONS
		=======
	*/
	// ChopperControls

	playWholeVideo: function(e) {
		// since this function may not be available even after the youtube player
		// is instantiated (video may have to load first), check first if the function
		// is defined; if it is not, this action will not play the video
		if (!this.state.isPlayerReady) {
			return;
		}
		this.setState({
			hasChopStarted: false,
			hasChopReachedEnd: false,
			hasWholeVideoStarted: true
		}, function() {
			this.player.loadVideoById(
				extractYoutubeIdFromUrl(this.state.video.video_url)
			);
		});
	},

	startPlayerFromChop: function(e) {
		if (!this.state.isPlayerReady) {
			return;
		}
		activeChop = this.state.activeChop;

		this.setState({
			hasChopStarted: true,
			hasWholeVideoStarted: false
		}, function() {
			this.player.loadVideoById({
				'videoId': extractYoutubeIdFromUrl(this.state.video.video_url),
				'startSeconds': activeChop.start / 1000,
				'endSeconds': activeChop.end / 1000
			});
		}.bind(this));
	},

	stopPlayer: function(e) {
		// hasChopReachedEnd must be set to false, so that if a chop has ended,
		// and a new one is selected, the app knows a new chop is playing,
		// rather than restarting a chop
		// note that this action is called from  selectActiveChop()
		this.setState({
			hasChopStarted: false,
			hasChopReachedEnd: false,
			hasWholeVideoStarted: false,
		}, function() {
			this.player.stopVideo();
		});
	},

	// ChopperOrganizer
	setUpChopsData: function() {
		ChopUtils.getAll(this.state.videoId)
			.then(function(resp) {
				if (!resp) {
					this.setState({
						chopIndexes: null,
						chops: null,
						isLoadingChopData: false,
						isSavingChopData: false
					});
					return;
				}

				var chops_arr = resp;
				chops = {}
				chops_arr.forEach(function(chop) {
					chops[chop.id] = chop;
				});
				chopIndexes = chops_arr.map(function(chop) {
					return chop.id;
				});

				this.resetInputs();

				this.setState({
					chopIndexes: chopIndexes,
					chops: chops,
					editedChop: null,
					createdChopsKeys: {},
					updatedChopsKeys: {},
					deletedChopsKeys: {},
					isLoadingChopData: false,
					isSavingChopData: false
				});
			}.bind(this));
	},

	setUpVideoData: function(callback) {
		VideoUtils.getVideo(this.state.videoId)
			.then(function(resp) {
				if (!resp) {
					this.setState({
						video: null
					});
					return;
				}

				this.setState({
					video: resp
				}, callback);
			}.bind(this));
	},

	saveChops: function(e) {
		this.setState({
			isSavingChopData: true
		});

		var createdData = [];
		var updatedData = [];
		var deletedData = [];

		// created data
		if (Object.keys(this.state.createdChopsKeys).length > 0) {
			Object.keys(this.state.createdChopsKeys).forEach(function(x) {
				createdData.push(this.state.chops[x]);
			}.bind(this));
		}

		// updated data
		if (Object.keys(this.state.updatedChopsKeys).length > 0) {
			Object.keys(this.state.updatedChopsKeys).forEach(function(x) {
				updatedData.push(this.state.chops[x]);
			}.bind(this));
		}

		// deleted data
		if (Object.keys(this.state.deletedChopsKeys).length > 0) {
			Object.keys(this.state.deletedChopsKeys).forEach(function(x) {
				deletedData.push(x);
			}.bind(this));
		}

		ChopUtils.save(createdData, updatedData, deletedData, this.state.videoId)
			.then(function(success) {
				this.setUpChopsData();
				this.setAlertModalStates(e, 'savedChopsSuccess');
			}.bind(this))
	},

	// ChopperOrganizer -> ChopperInput

	resetInputs: function(e) {
		this.setState({
			chopNameInput: '',
			chopStartHoursInput: '',
			chopStartMinsInput: '',
			chopStartSecsInput: '',
			chopEndHoursInput: '',
			chopEndMinsInput: '',
			chopEndSecsInput: '',
		})
	},

	addChop: function(e) {
		var chops = this.state.chops;
		var chopIndexes = this.state.chopIndexes;
		var createdChopsKeys = this.state.createdChopsKeys;
		var newChopName = this.state.chopNameInput.trim();

		var startMillis = ChopUtils.convertTimeToMillis(
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartHoursInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartMinsInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartSecsInput) ),
		);
		var endMillis = ChopUtils.convertTimeToMillis(
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndHoursInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndMinsInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndSecsInput) ),
		);

		var newChop = {
			id: 'c' + Object.keys(createdChopsKeys).length,
			name: newChopName,
			start: startMillis,
			end: endMillis,
			videoId: this.state.videoId
		};

		createdChopsKeys[newChop.id] = newChop.id;
		chops[newChop.id] = newChop;
		chopIndexes.push(newChop.id)

		var newChopIndexes = this.sortChopIndexes(chops, chopIndexes);

		this.setState({
			createdChopsKeys: createdChopsKeys,
			chops: chops,
			chopIndexes: newChopIndexes,
		}, function() {
			this.setAlertModalStates(e, 'addedChopSuccess');
		});

		this.resetInputs(e);
	},

	cancelChopEdit: function(e) {
		this.setState({
			editedChop: null
		})

		this.resetInputs(e);
	},

	updateChop: function(e) {
		var updatedChopName = this.state.chopNameInput.trim();
		var chops = this.state.chops;
		var chopIndexes = this.state.chopIndexes;
		var createdChopsKeys = this.state.createdChopsKeys;
		var updatedChopsKeys = this.state.updatedChopsKeys;
		var editedChop = this.state.editedChop;

		var startMillis = ChopUtils.convertTimeToMillis(
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartHoursInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartMinsInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopStartSecsInput) ),
		);
		var endMillis = ChopUtils.convertTimeToMillis(
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndHoursInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndMinsInput) ),
			parseInt( ChopUtils.zeroEmptyInput(this.state.chopEndSecsInput) ),
		);

		var updatedChop = chops[editedChop.id];
		updatedChop.name = updatedChopName;
		updatedChop.start = startMillis;
		updatedChop.end = endMillis;

		// newly created Chops should never be considered "updated"
		if (!createdChopsKeys[updatedChop.id]) {
			updatedChopsKeys[updatedChop.id] = updatedChop.id;
		}

		var newChopIndexes = this.sortChopIndexes(chops, chopIndexes);

		this.setState({
			updatedChopsKeys: updatedChopsKeys,
			chops: chops,
			chopIndexes: newChopIndexes,
			editedChop: null
		}, function() {
			this.setAlertModalStates(e, 'updatedChopSuccess');
		});

		this.resetInputs(e);
	},

	deleteChop: function(e, confirmed) {
		if (!confirmed) {
			this.setConfirmationModalStates(
				e,
				type = 'deleteChop',
				callback = this.deleteChop,
				data = null
			);
			return;
		}

		var chops = this.state.chops;
		var chopIndexes = this.state.chopIndexes;
		var createdChopsKeys = this.state.createdChopsKeys;
		var updatedChopsKeys = this.state.updatedChopsKeys;
		var deletedChopsKeys = this.state.deletedChopsKeys;
		var activeChop = this.state.activeChop;
		var editedChop = this.state.editedChop;

		delete chops[editedChop.id];
		delete updatedChopsKeys[editedChop.id];

		// De-select active chop if it is the one being deleted
		if (activeChop && activeChop.id === editedChop.id) {
			this.setState({activeChop: null})
		}

		var index = chopIndexes.indexOf(editedChop.id);
		var slice1 = chopIndexes.slice(0, index);
		var slice2 = chopIndexes.slice(index+1, chopIndexes.length);
		var newChopIndexes = slice1.concat(slice2);

		// newly created Chops should never be considered "deleted"
		if (!createdChopsKeys[editedChop.id]) {
			deletedChopsKeys[editedChop.id] = editedChop.id;
		}
		delete createdChopsKeys[editedChop.id];

		this.setState({
			createdChopsKeys: createdChopsKeys,
			updatedChopsKeys: updatedChopsKeys,
			deletedChopsKeys: deletedChopsKeys,
			chops: chops,
			chopIndexes: newChopIndexes,
			editedChop: null
		}, function() {
			this.setAlertModalStates(e, 'deletedChopSuccess')
		})

		this.resetInputs(e);
	},

	updateChopperInput: function(e) {
		var dataField = e.target.getAttribute('data-field');
		var whichState;

		switch (dataField) {
			case 'name':
				whichState = 'chopNameInput';
				break;
			case 'startHours':
				whichState = 'chopStartHoursInput';
				break;
			case 'startMins':
				whichState = 'chopStartMinsInput';
				break;
			case 'startSecs':
				whichState = 'chopStartSecsInput';
				break;
			case 'endHours':
				whichState = 'chopEndHoursInput';
				break;
			case 'endMins':
				whichState = 'chopEndMinsInput';
				break;
			case 'endSecs':
				whichState = 'chopEndSecsInput';
				break;
		}

		// validate time characters
		if (whichState != 'chopNameInput') {
			var value = e.target.value;
			if (isNaN(value) && value != '') {
				return;
			}
		}

		var newState = {};
		newState[whichState] = e.target.value;

		// validate/set time limits
		if (whichState != 'chopNameInput') {
			if (e.target.value > 60) {
				newState[whichState] = 59;
			}
		}

		this.setState(newState);

	},

	// ChopperOrganizer -> ChopperList -> ChopperItem
	selectActiveChop: function(e, chop) {
		if (this.state.activeChop && this.state.activeChop.id === chop.id) {
			return;
		}

		if (this.state.isPlayerReady) {
			this.stopPlayer(e);
		}


		this.setState({
			activeChop: chop
		});
	},

	setEditedChop: function(e, chop) {
		e.stopPropagation();

		var startInputs = this.createTimeInputs(chop.start);
		var endInputs = this.createTimeInputs(chop.end);

		this.setState({
			editedChop: chop,
			chopNameInput: chop.name,
			chopStartHoursInput: startInputs.hours,
			chopStartMinsInput: startInputs.mins,
			chopStartSecsInput: startInputs.secs,
			chopEndHoursInput: endInputs.hours,
			chopEndMinsInput: endInputs.mins,
			chopEndSecsInput: endInputs.secs,
		});
	},

	setHoveredChop: function(e, chop) {
		if (e.type === 'mouseenter') {
			this.setState({
				hoveredChop: chop
			});
		}

		else if (e.type === 'mouseleave') {
			this.setState({
				hoveredChop: null
			});
		}
	},

	// Alert Modal
	closeAlertModal: function(e) {
		this.setState({
			shouldRenderAlertModal: false,
			alertMessage: '',
			alertClassExt: ''
		});
	},

	setAlertModalStates: function(e, type) {
		var classExt, msg;
		switch (type) {
			case 'addedChopSuccess':
				msg = 'Chop has been added.';
				classExt = "chopAdded";
				break;
			case 'updatedChopSuccess':
				msg = 'Chop has been updated.';
				classExt = "chopUpdated";
				break;
			case 'deletedChopSuccess':
				msg = 'Chop has been deleted.';
				classExt = "chopDeleted";
				break;
			case 'savedChopsSuccess':
				msg = 'Project has been saved.';
				classExt = 'chopsSaved';
				break;
		}

		this.setState({
			alertMessage: msg,
			alertClassExt: classExt,
			shouldRenderAlertModal: true
		});
	},


	// Confirmation Modal
	confirmConfirmationModal: function(e) {
		this.state.confirmationCallback(e, confirmed = true)
		this.closeConfirmationModal(e);
	},

	closeConfirmationModal: function(e) {
		this.setState({
			shouldRenderConfirmationModal: false,
			confirmationCallback: null,
			confirmationData: null,
			confirmationMessage: '',
			confirmationClassExt: '',
		});
	},

	setConfirmationModalStates: function(e, type, callback, data) {
		var classExt, msg;
		switch (type) {
			case 'deleteChop':
				msg = 'Delete this Chop permanently?';
				classExt = "deleteChop";
				break;
		}

		this.setState({
			confirmationMessage: msg,
			confirmationClassExt: classExt,
			confirmationCallback: callback,
			confirmationData: data,
			shouldRenderConfirmationModal: true
		});
	},

	/*
		=====================
		YOUTUBE PLAYER EVENTS
		=====================
	*/
	handlePlayerReady: function() {
		this.setState({
			isPlayerReady: true
		});
	},

	handlePlayerStateChange: function(e) {
		// If player begins with the default Youtube play button,
		// UI state must be synced
		if (e.data === YT.PlayerState.PLAYING) {
			if (!this.state.hasChopStarted && !this.state.hasWholeVideoStarted) {
				this.setState({
					hasChopStarted: false,
					hasChopReachedEnd: false,
					hasWholeVideoStarted: true,
				});
			}
		}
		else if (e.data === YT.PlayerState.BUFFERING) {
			//Case: chop out of range
			// If user seeks outside the Chop range, set hasChopStarted to false
			// because the  chop should be effectively "cancelled"
			// It follows that hasChopReachedEnd must also be reset to false
			// The PLAYING case of this function (which triggers right after)
			// will see that the chop is no longer playing, and the
			// hasWholeVideoStarted state will be set to true
			if (this.state.hasChopStarted) {
				var start = this.state.activeChop.start;
				var end = this.state.activeChop.end;
				var curr = this.player.getCurrentTime() * 1000;

				if ((curr < start) || (curr > end)) {
					this.setState({
						hasChopStarted: false,
						hasChopReachedEnd: false,
						hasWholeVideoStarted: true
					});
				}

			}
		}
		else if (e.data === YT.PlayerState.ENDED) {
			// Case: restarting a chop that ended
			// Be mindful of what this case actually covers
			// Since restarting a chop that ended will actually re-trigger the
			// ENDED state (either by replaying the Chop or the whole video),
			// this case handles that (non-intuitive) scenario
			// hasChopStarted should not be reset to false because it was just
			// set to true by the startPlayerFromChop action
			// hasChopReachedEnd should be reset to false since the chop has
			// started, not ended
			// This case effectively prevents the case below (see: reached end of chop)
			// from happening twice in a row
			if (this.state.hasChopStarted && this.state.hasChopReachedEnd) {
				this.setState({
					hasChopReachedEnd: false
				});
			}
			// Case: Reached the end of the chop
			// hasChopStarted is accordingly set to false
			// hasChopReachedEnd is set to true; other than this being obvious,
			// its main purpose is for the case above (see: restarting a chop that
			// ended); the ENDED state will be re-triggered if a chop is re-played,
			// and if that case were not there, this case would incorrectly execute
			// twice, falsely believing the chop has ended again
			else if (this.state.hasChopStarted) {
				this.setState({
					hasChopStarted: false,
					hasChopReachedEnd: true
				});
			}
			// Case: Reached the end of the video
			else {
				this.setState({
					hasChopStarted: false,
					hasWholeVideoStarted: false
				});
			}
		}
	},

	/*
		=======
		METHODS
		=======
	*/
	// Given a map of Chops and an ordered arrray of their indexes (keys),
	// function returns a new array of the chop indexes that are sorted
	// according to this container's states (orderBy, direction)
	sortChopIndexes: function(chops, chopIndexes) {
		var orderBy = this.state.orderBy;
		var direction = this.state.direction;

		chopIndexes.sort(function(a, b) {
			var keyA, keyB;
			var chopA = chops[a];
			var chopB = chops[b]

			if (orderBy === NAME) {
				keyA = chopA[name].toString().toUpperCase();
				keyB = chopB[name].toString().toUpperCase();
			}
			else if (orderBy === TIME) {
				keyA = chopA['start'].valueOf()
				keyB = chopB['start'].valueOf()
				keyA2 = chopA['end'].valueOf();
				keyB2 = chopB['end'].valueOf();
			}

			// start comparison
			if (keyA < keyB) {
				result = -1;
			}
			else if (keyA > keyB) {
				result = 1;
			}
			else {
				// end comparison
				if (keyA2 < keyB2) {
					result = -1;
				}
				else if (keyA2 > keyB2) {
					result = 1;
				}
				else {
					result = 0;
				}
			}

			if (direction === DESCENDING) {
				result *= -1;
			}

			return result;
		});

		return chopIndexes;
	},

	// Converts milliseconds to an object of hours, minutes, and seconds,
	// then returns the new object
	createTimeInputs: function(millis) {
		var total_secs = millis / 1000;
		var secs_remaining;

		var hours = parseInt(total_secs / 3600);

		secs_remaining = total_secs % 3600;
		var mins = parseInt(secs_remaining / 60);

		secs_remaining = secs_remaining % 60;
		var secs = parseInt(secs_remaining);

		return {
			hours: hours,
			mins: mins,
			secs: secs
		}
	},

	/*
		======
		RENDER
		======
	*/
	render: function() {

		chopInputStates = {
			name: this.state.chopNameInput,
			startHours: this.state.chopStartHoursInput,
			startMins: this.state.chopStartMinsInput,
			startSecs: this.state.chopStartSecsInput,
			endHours: this.state.chopEndHoursInput,
			endMins: this.state.chopEndMinsInput,
			endSecs: this.state.chopEndSecsInput,
		}

		// props
		var canSave = false;
		if ((Object.keys(this.state.createdChopsKeys).length > 0) ||
			(Object.keys(this.state.updatedChopsKeys).length > 0) ||
			(Object.keys(this.state.deletedChopsKeys).length > 0)) {
			canSave = true;
		}

		return (
			<div className="ChopperContainer">

				{(this.state.shouldRenderAlertModal)
					?
					<AlertModal
						closeModal={this.closeAlertModal}
						message={this.state.alertMessage}
						classExt={this.state.alertClassExt}
						shouldRender={this.state.shouldRenderAlertModal}
						/>
					:
					null
				}
				{(this.state.shouldRenderConfirmationModal)
					?
					<ConfirmationModal
						closeModal={this.closeConfirmationModal}
						confirmModal={this.confirmConfirmationModal}
						message={this.state.confirmationMessage}
						classExt={this.state.confirmationClassExt}
						shouldRender={this.state.shouldRenderConfirmationModal}
						/>
					:
					null
				}

				<div className="grid-wrapper-ChopperContainer">
					<div className="col col-ChopperOrganizer">
						<ChopperOrganizer
							addChop={this.addChop}
							cancelChopEdit={this.cancelChopEdit}
							deleteChop={this.deleteChop}
							saveChops={this.saveChops}
							selectActiveChop={this.selectActiveChop}
							setEditedChop={this.setEditedChop}
							setHoveredChop={this.setHoveredChop}
							updateChop={this.updateChop}
							updateChopperInput={this.updateChopperInput}

							activeChop={this.state.activeChop}
							hoveredChop={this.state.hoveredChop}
							canSave={canSave}
							chopInputStates={chopInputStates}
							chops={this.state.chops}
							chopIndexes={this.state.chopIndexes}
							editedChop={this.state.editedChop}
							isLoadingChopData={this.state.isLoadingChopData}
							isSavingChopData={this.state.isSavingChopData} />
					</div>{/*

				 */}<div className="col col-YTPlayerAndControls-wrapper">
						<div className="YTPlayerAndControls-wrapper">
							<div className="YTPlayer-wrapper">
								{(this.state.isPlayerReady)
									?
										null
									:
										<Loading />
								}
								<div
									className="YTiframe"
									ref={function (r) { this.playerRef = r }.bind(this)}>
								</div>

							</div>
							<ChopperControls
								continuePlayer={this.continuePlayer}
								pausePlayer={this.pausePlayer}
								playWholeVideo={this.playWholeVideo}
								startPlayerFromChop={this.startPlayerFromChop}
								activeChop={this.state.activeChop}
								hasChopStarted={this.state.hasChopStarted}
								hasWholeVideoStarted={this.state.hasWholeVideoStarted} />
						</div>
					</div>

				</div>
			</div>
		)
	}
});

module.exports = ChopperContainer;