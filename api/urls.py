from django.conf.urls import include, url
from django.conf.urls import url

from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.urlpatterns import format_suffix_patterns

from . import views


urlpatterns = [
    url(r'^videos/$',
        views.video_list,
        name='video-list'),
    url(r'^videos/(?P<pk>[0-9]+)/$',
        views.video_detail,
        name='video-detail'),
    url(r'^chops/$',
        views.chop_list,
        name='chop-list'),
    url(r'^chops/(?P<pk>[0-9]+)/$',
        views.chop_detail,
        name='chop-detail'),
    url(r'^chops/(?P<pk>\d+(,\d+)*)/$',
        views.chop_detail,
        name='chop-detail'),
    url(r'^users/$',
        views.user_list,
        name='user-list'),
    url(r'^users/(?P<pk>[0-9]+)/$',
        views.user_detail,
        name='user-detail'),
]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls',
                               namespace='rest_framework')),
    url(r'^api-token-auth/$', obtain_auth_token),
]

# quick tests
urlpatterns += [
    url(r'^token/$', views.token),
]

urlpatterns = format_suffix_patterns(urlpatterns)