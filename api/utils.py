import json

import requests

import isodate

# ==================
# Youtube API Helper
# ==================

API_KEY = 'AIzaSyAbVPVim_R-sYr3iDHbAZLATNh0cmHTwPs'

def get_id(video_url):
    substringIndex = video_url.find('youtube.com/watch?v=')
    idIndex = substringIndex + 20

    return (video_url[idIndex:])

def get_video_info(video_url):
    api_root = 'https://www.googleapis.com/youtube/v3/videos'
    params = '?part=id,snippet,contentDetails&id=' + get_id(video_url)
    key = '&key=' + API_KEY
    url = api_root + params + key

    response = requests.get(url)
    
    data = json.loads(response.text)

    return data

def get_video_url(data):
    id = data['items'][0]['id']
    return 'https://www.youtube.com/watch?v=' + id

def get_thumb_url(data):
    thumb_url = data['items'][0]['snippet']['thumbnails']['default']['url']
    return thumb_url

def get_length(data):
    length = data['items'][0]['contentDetails']['duration']
    return length

def format_length(raw_length):
    td = isodate.parse_duration(raw_length)
    formatted_length = int(td.seconds*1000 + td.microseconds/1000)
    return formatted_length