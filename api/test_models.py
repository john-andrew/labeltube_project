from django.contrib.auth.models import User
from django.test import TestCase

from .models import Video


class VideoModelTest(TestCase):

    def setUp(self):
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
        self.client.login(username='test_user', password='password')

    def test_new_video(self):
        test_user = User.objects.get(username='test_user')
        video = Video.objects.create(
            name='Video Name',
            video_url='https://www.youtube.com/watch?v=vQjmz9wCjLA',
            thumb_url='https://i.ytimg.com/vi/vQjmz9wCjLA/default.jpg',
            user=test_user
        )
        video.save()

        self.assertEqual(Video.objects.count(), 1)
        self.assertEqual(video.name, 'Video Name')
        self.assertEqual(video.video_url,
                         'https://www.youtube.com/watch?v=vQjmz9wCjLA')
        self.assertEqual(video.thumb_url,
                         'https://i.ytimg.com/vi/vQjmz9wCjLA/default.jpg')
        self.assertEqual(video.user, test_user)