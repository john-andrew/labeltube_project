import json

from django.contrib.auth.models import User
from django.shortcuts import render

from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse

from . import utils
from .models import Chop, Video
from .serializers import ChopSerializer, VideoSerializer, UserSerializer


# ================
# Helper Functions
# ================

# Given a request, extracts/converts the data into a form useable
# by DRF serializer, and returns that data
def get_put_data_from_request(request):
    unicode_data = request.body.decode('utf-8')
    put_data = json.loads(unicode_data)

    return put_data

# ====
# Root
# ====
@api_view(['GET'])
def api_root(request, format=None):
    return Response({
        'users': reverse('user-list', request=request, format=format),
        'videos': reverse('video-list', request=request, format=format),
        'chops': reverse('chop-list', request=request, format=format),
    })


# ===============
# Video Endpoints
# ===============
@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def video_list(request, format=None):
    context = {'request': request}

    if request.method == 'GET':
        qs = Video.objects.filter(user=request.user)
        s = VideoSerializer(qs, many=True, context=context)
        return Response(s.data)

    elif request.method == 'POST':
        yt_data = utils.get_video_info(request.data['video_url'])
        video_url = utils.get_video_url(yt_data)
        thumb_url = utils.get_thumb_url(yt_data)
        raw_length = utils.get_length(yt_data)
        formatted_length = utils.format_length(raw_length)

        context['video_url'] = video_url
        context['thumb_url'] = thumb_url
        context['length'] = formatted_length

        s = VideoSerializer(data=request.data, context=context)

        if s.is_valid():
            s.save(user=request.user)
            return Response(s.data, status=status.HTTP_201_CREATED)

        return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)  

@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((IsAuthenticated, ))
def video_detail(request, pk, format=None):
    context = {'request': request}

    try:
        q = Video.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
    if q.user.id != request.user.id:
        return Response(status=status.HTTP_403_FORBIDDEN)

    if request.method == 'GET':
        s = VideoSerializer(q, context=context)
        return Response(s.data)

    elif request.method == 'PUT':
        put_data = get_put_data_from_request(request)

        new_video_url = ''
        if 'video_url' in put_data:
            new_video_url = put_data['video_url']

        # todo: this must be changed to compare YT video ID only
        # Get new video and thumb url
        if (new_video_url) and (new_video_url != q.video_url):
            yt_data = utils.get_video_info(put_data.get('video_url'))
            video_url = utils.get_video_url(yt_data)
            thumb_url = utils.get_thumb_url(yt_data)
            raw_length = utils.get_length(yt_data)
            formatted_length = utils.format_length(raw_length)

        # Set context
        if new_video_url:
            context['video_url'] = video_url
            context['thumb_url'] = thumb_url
            context['length'] = formatted_length

        # Serialize
        s = VideoSerializer(q,
                            data=request.data,
                            context=context,
                            partial=True)
        if s.is_valid():
            s.save()
            return Response(s.data)
        return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)  

    elif request.method == 'DELETE':
        q.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


# ==============
# Chop Endpoints
# ==============
@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def chop_list(request, format=None):
    context = {'request': request}

    # get video
    if 'video_id' not in request.query_params:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    try:
        video_id = int(request.query_params['video_id'])
    except ValueError:
        return Response(status=status.HTTP_400_BAD_REQUEST)
    try:
        video = Video.objects.get(pk=video_id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # forbidden case
    if video.user != request.user:
         return Response(status=status.HTTP_403_FORBIDDEN)

    # get method
    if request.method == 'GET':
        qs = Chop.objects.filter(video=video)
        s = ChopSerializer(qs, many=True, context=context)
        return Response(s.data)

    # post method
    elif request.method == 'POST':
        s = ChopSerializer(data=request.data, context=context, many=True)

        if s.is_valid():
            s.save(video=video)
            return Response(s.data, status=status.HTTP_201_CREATED)

        return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'PUT', 'DELETE'])
@permission_classes((IsAuthenticated, ))
def chop_detail(request, pk, format=None):
    context = {'request': request}
    pks = pk.split(',')

    # get method
    if request.method == 'GET':
        # query (single)
        detail_pk = pks[0]
        try:
            q = Chop.objects.get(pk=detail_pk)
        except:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if q.video.user.id != request.user.id:
            return Response(status=status.HTTP_403_FORBIDDEN)

        s = ChopSerializer(q, context=context)
        return Response(s.data)

    # put method
    elif request.method == 'PUT':

        for p in pks:
            # query
            try:
                q = Chop.objects.get(pk=p)
            except:
                return Response(status=status.HTTP_404_NOT_FOUND)
            if q.video.user.id != request.user.id:
                return Response(status=status.HTTP_403_FORBIDDEN)

            # data
            data = None
            for obj in request.data:
                if obj['id'] == int(p):
                    data = obj
            if not data:
                continue

            # serialization
            s = ChopSerializer(q, data=data, context=context, partial=True)
            if s.is_valid():
                s.save()
            else:
                return Response(s.errors, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_200_OK)

    # delete method
    elif request.method == 'DELETE':

        for p in pks:
            # query
            try:
                q = Chop.objects.get(pk=p)
            except:
                return Response(status=status.HTTP_404_NOT_FOUND)
            if q.video.user.id != request.user.id:
                return Response(status=status.HTTP_403_FORBIDDEN)

            # delete
            q.delete()
            
        return Response(status=status.HTTP_204_NO_CONTENT)


# ==============
# User Endpoints
# ==============
@api_view(['GET', 'POST'])
# @permission_classes((IsAuthenticated, ))
def user_list(request, format=None):
    context = {'request': request}

    if request.method == 'GET':
        qs = User.objects.all()
        s = UserSerializer(qs, many=True, context=context)    
        return Response(s.data)

    elif request.method == 'POST':
        
        s = UserSerializer(data=request.data, context=context)

        try:
            duplicate_user = User.objects.get(username=request.data['username'])
        except:
            duplicate_user = None
        if duplicate_user:
            data = {'error': 'duplicate'}
            return Response(data, status=status.HTTP_400_BAD_REQUEST)

        if s.is_valid():
            s.save()
            return Response(s.data, status=status.HTTP_201_CREATED)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
@permission_classes((IsAuthenticated, ))
def user_detail(request, pk, format=None):
    context = {'request': request}

    try:
        q = User.objects.get(pk=pk)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
    
    s = UserSerializer(q, context=context)
    
    return Response(s.data)


# ===========
# Quick Tests
# ===========
@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticated, ))
def token(request, format=None):
    return Response("OK")