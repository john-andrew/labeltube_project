from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, URLValidator

from rest_framework import serializers

from .models import Chop, Video

class ChopSerializer(serializers.HyperlinkedModelSerializer):
    video = serializers.HyperlinkedRelatedField(view_name='video-detail',
                                                read_only=True)
    video_id = serializers.ReadOnlyField(source='video.id')

    class Meta:
        model = Chop
        fields = ('url', 'id', 'name', 'video', 'video_id', 'start', 'end', 'created')
        read_only_fields = ('video_id',)


class VideoSerializer(serializers.HyperlinkedModelSerializer):
    user = serializers.ReadOnlyField(source='user.username')

    class Meta:
        model = Video
        fields = ('url', 'id', 'name', 'length', 'video_url', 'thumb_url', 'chops', 'user', 'created', 'last_used')
        read_only_fields = ('length', 'thumb_url', 'chops',)

    def validate(self, data):
        if 'video_url' in self.context:
            data['video_url'] = self.context['video_url']
            URLValidator()(data['video_url'])

        if 'thumb_url' in self.context:
            data['thumb_url'] = self.context['thumb_url']
            URLValidator()(data['thumb_url'])

        if 'length' in self.context:
            data['length'] = self.context['length']
            length = data['length']
            if not isinstance(length, int):
                raise ValidationError(
                    _('%(length)s is not an integer'),
                    params={'value': length}
                )
            MinValueValidator(0)(length)
            
        return super(VideoSerializer, self).validate(data)

class UserSerializer(serializers.HyperlinkedModelSerializer):
    videos = serializers.HyperlinkedRelatedField(many=True,
                                                 view_name='video-detail',
                                                 read_only=True)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = User
        fields = ('url', 'id', 'username', 'password', 'videos')
        read_only_fields = ('videos',)
        write_only_fields = ('password',)