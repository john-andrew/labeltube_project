from unittest import mock

from django.conf import settings
from django.contrib.auth.models import User
from django.core.urlresolvers import resolve, reverse
from django.test import TestCase

from rest_framework.test import APIRequestFactory, force_authenticate

from . import utils
from .models import Chop, Video
from .views import chop_detail, chop_list, video_detail, video_list, user_list
from .serializers import ChopSerializer, VideoSerializer


DOMAIN_NAME = settings.DOMAIN_NAME
URLS = {
    'python_vid': 'https://www.youtube.com/watch?v=vQjmz9wCjLA',
    'python_thumb': 'https://i.ytimg.com/vi/vQjmz9wCjLY/default.jpg',
    'ruby_vid': 'https://www.youtube.com/watch?v=ll2j_wZ6wQY',
    'ruby_thumb': 'https://i.ytimg.com/vi/ll2j_wZ6wQY/default.jpg',
}
LENGTHS = {
    'python': 10958000,
    'ruby': 1580000,
}
RAW_LENGTHS = {
    'python': 'PT3H2M38S',
    'ruby': 'PT26M20S',
}



class VideoListViewTest(TestCase):
    def setUp(self):
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
        self.client.login(username='test_user', password='password')

        test_video = Video.objects.create(
            name='Video Name',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video.save()

        test_video2 = Video.objects.create(
            name='Video Name2',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video2.save()
        
    def test_url_routes_to_view(self):
        url = '/api/videos/'

        found = resolve(url)

        self.assertEqual(found.func, video_list)

    def test_reverse_name_routes_to_view(self):
        url = reverse('video-list')

        found = resolve(url)

        self.assertEqual(found.func, video_list)

    def test_requires_authentication(self):
        url = reverse('video-list')

        # Authenticated
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)

        # Not authenticated
        self.client.logout()

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 401)

    def test_gets_correct_items_for_user(self):
        url = reverse('video-list')
        test_user = User.objects.get(username="test_user")
        wrong_user = User.objects.create(username='wrong_user')
        wrong_user.set_password('password')
        wrong_user.save()
        test_video3 = Video.objects.create(
            name='Video Name3',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=wrong_user
        )
        test_video3.save()

        resp = self.client.get(url)
        
        qs = Video.objects.filter(user=test_user)
        self.assertEqual(len(resp.data), 2)
        for q in qs:
            self.assertEqual(q.user.id, test_user.id)
    
    @mock.patch.object(utils, "get_length")
    @mock.patch.object(utils, "get_thumb_url")
    @mock.patch.object(utils, "get_video_url")
    @mock.patch.object(utils, "get_video_info")
    def test_can_create_a_new_item(self,
                                   get_video_info,
                                   get_video_url,
                                   get_thumb_url,
                                   get_length):
        get_video_info.return_value = ({})
        get_video_url.return_value = (
            URLS['ruby_vid']
        )
        get_thumb_url.return_value = (
            URLS['ruby_thumb']
        )
        get_length.return_value = (
            RAW_LENGTHS['ruby']
        )

        url = reverse('video-list')
        test_user = User.objects.get(username="test_user")
        post_data = {
            'name': 'Video Name3',
            'video_url': URLS['ruby_vid']
        }

        self.client.post(url, data=post_data)

        new_video = Video.objects.get(name='Video Name3')
        self.assertEqual(Video.objects.count(), 3)
        self.assertEqual(new_video.name, 'Video Name3')
        self.assertEqual(new_video.length, LENGTHS['ruby'])
        self.assertEqual(new_video.video_url, URLS['ruby_vid'])
        self.assertEqual(new_video.thumb_url, URLS['ruby_thumb'])

    @mock.patch.object(utils, "get_length")
    @mock.patch.object(utils, "get_thumb_url")
    @mock.patch.object(utils, "get_video_url")
    @mock.patch.object(utils, "get_video_info")
    def test_returns_error_with_bad_data(self,
                                   get_video_info,
                                   get_video_url,
                                   get_thumb_url,
                                   get_length):
        get_video_info.return_value = ({})
        get_video_url.return_value = (
            URLS['ruby_vid']
        )
        get_thumb_url.return_value = (
            URLS['ruby_thumb']
        )
        get_length.return_value = (
            RAW_LENGTHS['ruby']
        )

        url = reverse('video-list')
        test_user = User.objects.get(username="test_user")
        post_data_no_name = {
            'name': '',
            'video_url': URLS['ruby_vid']
        }
        post_data_no_video_url = {
            'name': '',
            'video_url': URLS['ruby_vid']
        }

        resp1 = self.client.post(url, data=post_data_no_name)
        resp2 = self.client.post(url, data=post_data_no_video_url)

        self.assertEqual(resp1.status_code, 400)
        self.assertEqual(resp2.status_code, 400)


class VideoDetailViewTest(TestCase):

    def get_view(self):
        return video_detail

    def setUp(self):
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
        self.client.login(username='test_user', password='password')

        test_video = Video.objects.create(
            name='Video Name',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video.save()

        test_video2 = Video.objects.create(
            name='Video Name2',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video2.save()

    def test_url_routes_to_view(self):
        url1 = '/api/videos/1/'
        url2 = '/api/videos/2/'

        found1 = resolve(url1)
        found2 = resolve(url2)

        self.assertEqual(found1.func, video_detail)
        self.assertEqual(found2.func, video_detail)

    def test_requires_authentication(self):
        url = '/api/videos/1/'

        # Authenticated
        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 200)

        # Not authenticated
        self.client.logout()

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 401)

    def test_returns_error_if_item_does_not_exist(self):
        url = '/api/videos/3/'

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)

    def test_returns_error_if_user_accesses_forbidden_item(self):
        url = '/api/videos/2/'
        wrong_user = User.objects.create(username='wrong_user')
        wrong_user.set_password('password')
        wrong_user.save()
        self.client.logout()
        self.client.login(username='wrong_user', password='password')

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 403)

    def test_gets_correct_item_for_user(self):
        url = '/api/videos/1/'
        test_user = User.objects.get(username="test_user")

        resp = self.client.get(url)
        
        d = resp.data
        self.assertEqual(d['name'], 'Video Name')
        self.assertEqual(d['id'], 1)

    @mock.patch.object(utils, "get_length")
    @mock.patch.object(utils, "get_thumb_url")
    @mock.patch.object(utils, "get_video_url")
    @mock.patch.object(utils, "get_video_info")
    def test_can_update_item(self,
                             get_video_info,
                             get_video_url,
                             get_thumb_url,
                             get_length):
        get_video_info.return_value = ({})
        get_video_url.return_value = (
            URLS['python_vid']
        )
        get_thumb_url.return_value = (
            URLS['python_thumb']
        )
        get_length.return_value = (
            RAW_LENGTHS['python']
        )

        view = self.get_view()
        url = '/api/videos/1/'
        pk = 1
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = {'id': 1, 'name': 'New Video Name'}
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        updated_video = Video.objects.get(id=1)
        self.assertEqual(updated_video.name, 'New Video Name')

    @mock.patch.object(utils, "get_length")
    @mock.patch.object(utils, "get_thumb_url")
    @mock.patch.object(utils, "get_video_url")
    @mock.patch.object(utils, "get_video_info")
    def test_updating_video_url_changes_dependent_fields(self,
                                                         get_video_info,
                                                         get_video_url,
                                                         get_thumb_url,
                                                         get_length):
        get_video_info.return_value = ({})
        get_video_url.return_value = (
            URLS['ruby_vid']
        )
        get_thumb_url.return_value = (
            URLS['ruby_thumb']
        )
        get_length.return_value = (
            RAW_LENGTHS['ruby']
        )

        view = self.get_view()
        pk = 1
        url = '/api/videos/1/'
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = {'id': 1,
                    'video_url': URLS['ruby_vid']}
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        updated_video = Video.objects.get(id=1)
        self.assertEqual(updated_video.length,
                         LENGTHS['ruby'])
        self.assertEqual(updated_video.video_url,
                         URLS['ruby_vid'])
        self.assertEqual(updated_video.thumb_url,
                         URLS['ruby_thumb'])


class ChopListViewTest(TestCase):

    def get_view(self):
        return chop_list

    def setUp(self):
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
        self.client.login(username='test_user', password='password')

        test_video = Video.objects.create(
            name='Video Name',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video.save()

        test_video2 = Video.objects.create(
            name='Video Name2',
            length=LENGTHS['ruby'],
            video_url=URLS['ruby_vid'],
            thumb_url=URLS['ruby_thumb'],
            user=test_user
        )
        test_video2.save()

        test_chop = Chop.objects.create(
            name='Chop Name',
            video=test_video,
            start=0,
            end=9
        )
        test_chop.save()

        test_chop2 = Chop.objects.create(
            name='Chop Name2',
            video=test_video,
            start=10,
            end=19
        )
        test_chop2.save()

        test_chop3 = Chop.objects.create(
            name='Chop Name3',
            video=test_video2,
            start=0,
            end=9
        )
        test_chop3.save()

    def test_url_routes_to_view(self):
        url = '/api/chops/'

        found = resolve(url)

        self.assertEqual(found.func, chop_list)

    def test_reverse_name_routes_to_view(self):
        url = reverse('chop-list')

        found = resolve(url)

        self.assertEqual(found.func, chop_list)

    def test_requires_authentication(self):
        url = reverse('chop-list')
        self.client.logout()

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 401)

    def test_gets_correct_items_for_user_and_video(self):
        test_user = User.objects.get(username="test_user")
        wrong_user = User.objects.create(username='wrong_user')
        wrong_user.set_password('password')
        wrong_user.save()
        test_video3 = Video.objects.create(
            name='Video Name3',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=wrong_user
        )
        test_video3.save()
        test_chop4 = Chop.objects.create(
            name='Chop Name4',
            video=test_video3,
            start=0,
            end=9
        )
        test_chop4.save()

        view = self.get_view()
        url = reverse('chop-list') + '?video_id=1'
        factory = APIRequestFactory()
        request = factory.get(url, format='json')
        force_authenticate(request, user=test_user)
        
        resp = view(request)
        
        test_video = Video.objects.get(pk=1)
        qs = Chop.objects.filter(video=test_video)        
        self.assertEqual(len(resp.data), 2)
        for q in qs:
            self.assertEqual(q.video.id, test_video.id)
            self.assertEqual(q.video.user.id, test_user.id)

    def test_requires_correct_user_for_video_parameter(self):
        wrong_user = User.objects.create(username='wrong_user')
        wrong_user.set_password('password')
        wrong_user.save()
        self.client.logout()
        self.client.login(username=wrong_user, password='password')

        view = self.get_view()
        url = reverse('chop-list') + '?video_id=1'
        factory = APIRequestFactory()
        request = factory.get(url, format='json')
        force_authenticate(request, user=wrong_user)
        
        resp = view(request)

        self.assertEqual(resp.status_code, 403)

    def test_returns_status_error_if_no_video_parameter(self):
        view = self.get_view()
        url = reverse('chop-list') + ''
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.get(url, format='json')
        force_authenticate(request, user=test_user)
        
        resp = view(request)

        self.assertEqual(resp.status_code, 400)

    def test_returns_staus_error_if_bad_video_parameter(self):
        view = self.get_view()
        url = reverse('chop-list') + '?video_id=asdf'
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.get(url, format='json')
        force_authenticate(request, user=test_user)
        
        resp = view(request)

        self.assertEqual(resp.status_code, 400)

    def test_returns_staus_error_if_video_not_found(self):
        view = self.get_view()
        url = reverse('chop-list') + '?video_id=10'
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.get(url, format='json')
        force_authenticate(request, user=test_user)
        
        resp = view(request)

        self.assertEqual(resp.status_code, 404)

    def test_can_create_a_new_item(self):
        test_user = User.objects.get(username="test_user")
        view = self.get_view()
        url = reverse('chop-list') + '?video_id=1'
        factory = APIRequestFactory()
        post_data = [
            {
                'name': 'Chop Name4',
                'video_id': 1,
                'start': 20,
                'end': 29,
            }            
        ]
        request = factory.post(url, data=post_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request)

        new_chop = Chop.objects.get(pk=4)
        self.assertEqual(new_chop.name, 'Chop Name4')
        self.assertEqual(new_chop.video.id, 1)
        self.assertEqual(new_chop.start, 20)
        self.assertEqual(new_chop.end, 29)

    def test_can_create_multiple_items(self):
        test_user = User.objects.get(username="test_user")
        view = self.get_view()
        url = reverse('chop-list') + '?video_id=1'
        factory = APIRequestFactory()
        post_data = [
            {
                'name': 'Chop Name4',
                'video_id': 1,
                'start': 20,
                'end': 29,
            },
            {
                'name': 'Chop Name5',
                'video_id': 1,
                'start': 20,
                'end': 29,
            }  
        ]
        request = factory.post(url, data=post_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request)

        new_chop = Chop.objects.get(pk=4)
        self.assertEqual(new_chop.name, 'Chop Name4')
        self.assertEqual(new_chop.video.id, 1)
        self.assertEqual(new_chop.start, 20)
        self.assertEqual(new_chop.end, 29)
        new_chop2 = Chop.objects.get(pk=5)
        self.assertEqual(new_chop2.name, 'Chop Name5')
    
    def test_returns_error_if_post_data_is_bad(self):
        test_user = User.objects.get(username="test_user")
        view = self.get_view()
        url = reverse('chop-list') + '?video_id=1'
        factory = APIRequestFactory()
        post_data = {
            'name': 'Chop Name4',
            'video_id': 1,
            'end': 29,            
        }
        request = factory.post(url, data=post_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request)

        self.assertEqual(resp.status_code, 400)


class ChopListDetailTest(TestCase):

    def get_view(self):
        return chop_detail

    def setUp(self):
        # test_user owns videos 1-2, chops 1-3
        # diff_user owns videos 3, chops 4
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
        self.client.login(username='test_user', password='password')

        test_video = Video.objects.create(
            name='Video Name',
            length=LENGTHS['python'],
            video_url=URLS['python_vid'],
            thumb_url=URLS['python_thumb'],
            user=test_user
        )
        test_video.save()

        test_video2 = Video.objects.create(
            name='Video Name2',
            length=LENGTHS['ruby'],
            video_url=URLS['ruby_vid'],
            thumb_url=URLS['ruby_thumb'],
            user=test_user
        )
        test_video2.save()

        test_chop = Chop.objects.create(
            name='Chop Name',
            video=test_video,
            start=0,
            end=9
        )
        test_chop.save()

        test_chop2 = Chop.objects.create(
            name='Chop Name2',
            video=test_video,
            start=10,
            end=19
        )
        test_chop2.save()

        test_chop3 = Chop.objects.create(
            name='Chop Name3',
            video=test_video2,
            start=0,
            end=9
        )
        test_chop3.save()

        diff_user = User.objects.create(username='diff_user')
        diff_user.set_password('password')
        diff_user.save()

        test_video3 = Video.objects.create(
            name='Video Name2',
            length=LENGTHS['ruby'],
            video_url=URLS['ruby_vid'],
            thumb_url=URLS['ruby_thumb'],
            user=diff_user
        )
        test_video3.save()

        test_chop4 = Chop.objects.create(
            name='Chop Name4',
            video=test_video3,
            start=0,
            end=9
        )
        test_chop4.save()

    def test_url_routes_to_view(self):
        url1 = '/api/chops/1/'
        url2 = '/api/chops/2/'

        found1 = resolve(url1)
        found2 = resolve(url2)

        self.assertEqual(found1.func, chop_detail)
        self.assertEqual(found2.func, chop_detail)

    def test_url_routes_to_view_for_multiple_pks(self):
        url1 = '/api/chops/1,2,3/'

        found1 = resolve(url1)

        self.assertEqual(found1.func, chop_detail)

    def test_reverse_name_routes_to_view(self):
        url1 = reverse('chop-detail', kwargs={'pk': 1})
        url2 = reverse('chop-detail', kwargs={'pk': 2})

        found1 = resolve(url1)
        found2 = resolve(url2)

        self.assertEqual(found1.func, chop_detail)
        self.assertEqual(found2.func, chop_detail)

    def test_reverse_name_routes_to_view_for_multiple_pks(self):
        url1 = reverse('chop-detail', kwargs={'pk': '1,2,3'})

        found1 = resolve(url1)

        self.assertEqual(found1.func, chop_detail)

    def test_requires_authentication(self):
        url = reverse('chop-detail', kwargs={'pk': 1})
        self.client.logout()

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 401)

    def test_returns_error_if_item_does_not_exist(self):
        url = reverse('chop-detail', kwargs={'pk': 5})

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 404)

    def test_returns_error_if_user_accesses_forbidden_item(self):
        url = reverse('chop-detail', kwargs={'pk': 1})
        wrong_user = User.objects.create(username='wrong_user')
        wrong_user.set_password('password')
        wrong_user.save()
        self.client.logout()
        self.client.login(username='wrong_user', password='password')

        resp = self.client.get(url)

        self.assertEqual(resp.status_code, 403)

    def test_gets_correct_item_for_user(self):
        url = reverse('chop-detail', kwargs={'pk': '1'})

        resp = self.client.get(url)
        
        d = resp.data
        self.assertEqual(d['name'], 'Chop Name')
        self.assertEqual(d['id'], 1)

    def test_can_update_item(self):
        view = self.get_view()
        pk = '1'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = [{'id': 1, 'name': 'New Chop Name'}]
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        updated_chop = Chop.objects.get(id=1)
        self.assertEqual(updated_chop.name, 'New Chop Name')

    def test_can_update_multiple_items(self):
        view = self.get_view()
        pk = '1,2,4'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = [{'id': 1, 'name': 'New Chop Name'},
                    {'id': 2, 'name': 'New Chop Name2'}]
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        self.assertEqual(resp.status_code, 403)

    def test_cannot_update_items_from_another_user(self):
        view = self.get_view()
        pk = '1,2'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = [{'id': 1, 'name': 'New Chop Name'},
                    {'id': 2, 'name': 'New Chop Name2'}]
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

    def test_rejects_bad_put_data(self):
        view = self.get_view()
        pk = '1'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        put_data = [{'id': 1, 'start': 'bad data'}]
        request = factory.put(url, data=put_data, format='json')
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        self.assertEqual(resp.status_code, 400)

    def test_can_delete_one_item(self):
        view = self.get_view()
        pk = '1'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.delete(url)
        force_authenticate(request, user=test_user)
        # exception_msg = 'Chop matching query does not exist.'

        resp = view(request, pk)

        self.assertEqual(resp.status_code, 204)

        # with self.assertRaises(Chop.DoesNotExist) as context:
        #     Chop.objects.get(id=1)
        # err = context.exception
        # self.assertEqual(str(err), exception_msg)

        self.assertEqual(len(Chop.objects.filter(id=1)), 0)

    def test_can_delete_multiple_items(self):
        view = self.get_view()
        pk = '1,2'
        del_filter = [1,2]
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.delete(url)
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        self.assertEqual(resp.status_code, 204)
        self.assertEqual(len(Chop.objects.filter(id__in=del_filter)), 0)

    def test_cannot_delete_items_from_another_user(self):
        view = self.get_view()
        pk = '1,2,4'
        url = reverse('chop-detail', kwargs={'pk': pk})
        test_user = User.objects.get(username="test_user")
        factory = APIRequestFactory()
        request = factory.delete(url)
        force_authenticate(request, user=test_user)

        resp = view(request, pk)

        self.assertEqual(resp.status_code, 403)

class UserListViewlTest(TestCase):

    def get_view(self):
        return user_list

    def setUp(self):
        test_user = User.objects.create(username='test_user')
        test_user.set_password('password')
        test_user.save()
    
    def test_url_routes_to_view(self):
        url1 = '/api/users/'

        found1 = resolve(url1)

        self.assertEqual(found1.func, user_list)

    def test_can_create_a_new_item(self):
        test_user = User.objects.get(username="test_user")
        view = self.get_view()
        url = reverse('user-list')
        factory = APIRequestFactory()
        post_data = {
                'username': 'test_user2',
                'password': 'password'
            }            

        request = factory.post(url, data=post_data, format='json')

        resp = view(request)

        new_user = User.objects.get(pk=2)
        self.assertEqual(new_user.username, 'test_user2')