# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_chop'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='length',
            field=models.PositiveIntegerField(default=0),
            preserve_default=False,
        ),
    ]
