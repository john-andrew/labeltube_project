# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Chop',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('name', models.CharField(max_length=100)),
                ('start', models.PositiveIntegerField()),
                ('end', models.PositiveIntegerField()),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('video', models.ForeignKey(to='api.Video', related_name='chops')),
            ],
            options={
                'ordering': ('start', 'end', 'name', 'video', '-created'),
            },
        ),
    ]
