from django.contrib.auth.models import User
from django.db import models


class Video(models.Model):
    name = models.CharField(max_length=100)
    length = models.PositiveIntegerField()
    video_url = models.URLField(max_length=200)
    thumb_url = models.URLField(max_length=100)
    user = models.ForeignKey(User,
                             related_name='videos',
                             on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    last_used = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return '%s by %s' %(self.name, self.user.username)


class Chop(models.Model):
    name = models.CharField(max_length=100)
    video = models.ForeignKey(Video,
                              related_name='chops',
                              on_delete=models.CASCADE)
    start = models.PositiveIntegerField()
    end = models.PositiveIntegerField()
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('start', 'end', 'name', 'video', '-created')


    def __str__(self):
        return '%s for %s by %s\n start: %d   end: %d ' %(self.name,
                                                          self.video.name,
                                                          self.video.user.username,
                                                          self.start,
                                                          self.end)