from django.contrib import admin

from .models import Chop, Video


class ChopAdmin(admin.ModelAdmin):
    fields = ('id', 'name', 'video', 'start', 'end', 'created',)
    list_display = ('id', 'name', 'video', 'start', 'end', 'created')
    readonly_fields = ('id', 'created')

class VideoAdmin(admin.ModelAdmin):
    fields = ('id', 'name', 'video_url', 'thumb_url', 'length', 'user', 'created', 'last_used',)
    list_display = ('id', 'name', 'user', 'created', 'last_used',)
    readonly_fields=('id', 'created', 'last_used', 'length',)

admin.site.register(Video, VideoAdmin)
admin.site.register(Chop, ChopAdmin)