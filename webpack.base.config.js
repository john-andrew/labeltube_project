var path = require('path')
var webpack = require('webpack')


module.exports = {
  context: __dirname,

  entry: {
    // note: here, 'index' will be the name of bundle to be rendered
    // in Django template, use: `{% render_bundle 'index' %}`
    index: ['babel-polyfill', './assets/js/index.js']
  },
    
	output: {
		path: path.resolve('./assets/bundles/'),
	},
    
	plugins: [
	],
	
	module: {
		loaders: [
		]
	},

	resolve: {
		modules: ['node_modules'],
		extensions: ['.js', '.jsx']
	}
}