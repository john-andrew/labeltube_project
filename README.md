# README #

### Last updated: 8/10, 4:00PM EST ### 

### How to Use ChopTube ###

The premise is quite simple:

* Create an account and log-in, or simply log-in as a Guest.

* Give a name for your video project and a URL to the Youtube video it will belong to; then click the "Add Video" button.

* Click the "Project" button for your video to get started on chopping up the video.

* On the top-left are inputs for a Chop name, a start-time, and end-time.  This will define a segment, or "chop," of the video.

* To play your chop, click on the "Chop" play button underneath the video.  It will play _only the time segment_ you have defined.

* You can click the "Video" play button if you want to _watch the whole video_ (or simply play the Youtube video as you normally would).

Note: By default, Chops are sorted in ascending order by start time (primary order), then end time (secondary order).  
The code, however, is designed to handle different ordering rules.

Note: All chop data will not be saved to the database unless you click the "Save" button.

### What is ChopTube ###

ChopTube was conceived as an application to chop up a Youtube video into multiple segments and allow for re-sequencing.

My original, personal use-case was for breaking down videos into labelled pieces for the purposes of dance,
hence the original project name of "LabelTube."

The idea of re-sequencing was inspired by a Twitter conversation that made me think about re-imagining video and GIFs as non-linear or combinatorial.

Unfortunately the re-sequencing part is yet to be implemented.  However, the app is otherwise fully-functional.

This application so far has largely been an exercise in building and delivering a ReactJS app with a non-trivial UI.

### A note on the Youtube player controls ###

Developing the video controls ended up being much more difficult than I expected because of the way the Youtube player manages its internal state.

The challenge lied in syncing my application state with the Youtube player state. I decided to keep the default YT player controls as a welcomed UX challenge, assuming that we as users prefer to use things in familiar ways. This proved to be difficult because the app state had to respond to the YT player's internal state, which didn't mesh well with the state pattern I followed (functional, one-way data flow).

Rough example: Whereas the app knows exactly what triggers a "pause state" if you click the app's "Pause" button, there are at least four ways that the YT's internal state might be in its own "pause state" with no obvious way of knowing how it was triggered. Situations like these snowballed in complexity because the YT player state could change unpredictably, and my app's stated needed to reflect both the YT player state and how it was triggered.

At one point I drew an unnecessarily complex finite state machine because of how difficult it was to reason through the state.

In the end, I ended up simplifying the controls. There were still a few edge cases to iron out, but it was much more sane. I made this decision because the state management became too unwieldy and didn't fit React's paradigm well.

I'm sure if I think hard enough, there'd be a simpler solution, but I suspect that having a suite of custom controls, including a custom time bar and sliders for seeking/setting start- and end-times would have worked much better than integrating with the YT player's controls. The YT player controls don't really mesh or flow well with the concept of the app, and in its current state, I think the interface is a little unintuitive.

### Feature, not a bug ###

Regarding the ad bug:

It wasn't until later when I kept on testing my code on the same video, that I started to get an ads from it.

My ad-blocker and other similar browser extensions were turned off to avoid clogging up my console when debugging.

It took me a while to realize that I was getting popup ads, and that's why halfway into development,
my Chops suddenly started replaying for no apparent reason.  It was the ads.

Jokingly, perhaps this is a "feature, not a bug" on Google's end, so that Google gets more ad play.
I guess they have no incentive to change this behavior.