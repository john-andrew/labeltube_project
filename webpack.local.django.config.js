var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var config = require('./webpack.local.config.js')

// configure for Django webpack loader
var HtmlWebpackPlugin = require('html-webpack-plugin')
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
  template: __dirname + '/templates/index.html',
  filename: 'index.html',
  inject: 'body'
});

config.output['filename'] = '[name]-[hash].js';

config.plugins = config.plugins.concat([
	HTMLWebpackPluginConfig,
	
	// globals
	new webpack.DefinePlugin({
		'process.env': {
			'SERVER_ENV': JSON.stringify('local-django'),
			'BASE_API_URL': JSON.stringify('http://127.0.0.1:8000/api/'),
	}}),

]);

module.exports = config;