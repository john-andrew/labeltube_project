var path = require('path')
var webpack = require('webpack')
var BundleTracker = require('webpack-bundle-tracker')

var HtmlWebpackPlugin = require('html-webpack-plugin')
var HTMLWebpackPluginConfig = new HtmlWebpackPlugin({
	// uncomment /react_labeltube/... for webpack dev server html file
  // template: __dirname + '/react_labeltube/index.html',
	// uncomment /templates/... for Django server html file
  template: __dirname + '/templates/index.html',
  filename: 'index.html',
  inject: 'body'
});

module.exports = {
  context: __dirname,

  entry: {
    // note: here, 'index' will be the name of bundle to be rendered
    // in Django template, use: `{% render_bundle 'index' %}`
    index: './assets/js/index.js',
  },
    
	output: {
		path: path.resolve('./assets/bundles/'),
		// uncomment '[name]-[hash].js for Django server'
		filename: '[name]-[hash].js', 
		// uncomment 'index_bundle.js' for webpack dev server
		// filename: 'index_bundle.js'
	},
    
	plugins: [
		new BundleTracker({filename: './webpack-stats.json'}),
		HTMLWebpackPluginConfig,
	],
	
	module: {
		loaders: [
			{test: /\.jsx?$/, exclude: /node_modules/, loader: 'babel-loader',},
			{test: /\.css$/, exclude: /node_modules/, loader: 'css-loader',},
		]
	},

	resolve: {
		modules: ['node_modules'],
		extensions: ['.js', '.jsx']
	}
}